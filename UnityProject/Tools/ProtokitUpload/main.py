# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import pathlib
import os
import sys

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('excute error!')
        sys.exit(1)

    app_version = sys.argv[1].strip()
    res_version = sys.argv[2].strip()

    print('Start upload current file , app_version : %s , res_version : %s .' % (app_version,res_version))

    argv = ['ProtokitGoUploader.py',
            'D:/GitProjects/sandwich-sample-conf',
            'android',
            '.x,.zip',
            'D:/TestUpload/UploadFolder',
            'samples',
            app_version,
            res_version,
            "true"
            ]
    import ProtokitGoUploader
    ProtokitGoUploader.excute(argv)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
