local protoc ={schemaMap={}}

function protoc:load(schema,name)
    self.schemaMap[name] = schema
    return true
end

return protoc