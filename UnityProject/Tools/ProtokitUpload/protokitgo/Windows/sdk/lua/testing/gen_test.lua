package.path ="../../../sample/gen/lua/?.lua;../?.lua;".. package.path

local testing = require 'testing'

local tprint = require 'print_r'
local pb = require 'pb'
local pb = require 'protoc'

require 'init'
gen:initAll()

testing.marshal = function ()
    msg.PACKAGE_NAME=""
    local hello1 = msg.HelloRequest1:new()
    hello1.name = "test"
    local packet,bytes = netutils:marshalRawPacket(hello1,nil,netutils.metadata.fromPairs("test-md-key","test-md-val"))
    local ret = netutils:unmarshalRawPacket(bytes)


    tprint(debug.getinfo (debug.getinfo(1,pb.encode).func, "f"))
    testing.equal(ret.uri,hello1:getMessageName())
    testing.equal(ret.message,hello1)
    testing.is_true(packet.rawAny.passThrough ~= "")
    testing.equal(ret.md:getFirst("test-md-key"),"test-md-val")
end