import sys
import os
import pathlib
import subprocess

VCS_TYPE = None
REPOSITORY_REMOTE_PATH = None
REPOSITORY_BRANCH_NAME = None
LOCAL_REPOSITORY_PARENT_PATH = None
LOCAL_REPOSITORY_DIR_NAME = None

COMMAND_LINE_ARGS_COUNT = 6


def _cli_error():
    print('The command line args count is not %d !' % COMMAND_LINE_ARGS_COUNT)
    sys.exit(1)


def parser_commandline_args(argv):
    print(argv)
    if len(argv) != COMMAND_LINE_ARGS_COUNT:
        _cli_error()

    global VCS_TYPE
    global REPOSITORY_REMOTE_PATH
    global REPOSITORY_BRANCH_NAME
    global LOCAL_REPOSITORY_PARENT_PATH
    global LOCAL_REPOSITORY_DIR_NAME

    VCS_TYPE = argv[1]
    print('VCS_TYPE : %s ' % VCS_TYPE)
    REPOSITORY_REMOTE_PATH = argv[2]
    print('REPOSITORY_REMOTE_PATH : %s ' % REPOSITORY_REMOTE_PATH)
    REPOSITORY_BRANCH_NAME = argv[3]
    print('REPOSITORY_BRANCH_NAME : %s ' % REPOSITORY_BRANCH_NAME)
    LOCAL_REPOSITORY_PARENT_PATH = argv[4]
    print('LOCAL_REPOSITORY_PATH : %s ' % LOCAL_REPOSITORY_PARENT_PATH)
    LOCAL_REPOSITORY_DIR_NAME = argv[5]
    print('LOCAL_REPOSITORY_DIR_NAME : %s' % LOCAL_REPOSITORY_DIR_NAME)


def execute_cmd(cmd, cwd_dir, error_log):
    try:
        subprocess.run(cmd, cwd=cwd_dir)
    except Exception as ex:
        print(error_log)
        raise ex


def clone_repo_internal(cwd_dir):
    print('start clone repository that remote path is \"%s\"' % REPOSITORY_REMOTE_PATH)
    cmd = None
    if VCS_TYPE == 'GIT':
        cmd = ['git', 'clone', REPOSITORY_REMOTE_PATH, LOCAL_REPOSITORY_DIR_NAME]
    else:
        cmd = ['svn', 'checkout', REPOSITORY_REMOTE_PATH, LOCAL_REPOSITORY_DIR_NAME]
    execute_cmd(cmd, LOCAL_REPOSITORY_PARENT_PATH, 'clone repository failure!')
    switch_branch(cwd_dir=cwd_dir)


def switch_branch(cwd_dir):
    print('Start switch branch : \"%s\".' % REPOSITORY_BRANCH_NAME)
    if REPOSITORY_BRANCH_NAME == 'None':
        print('Skip switch branch')
        return
    if VCS_TYPE == 'GIT':
        cmd = ['git', 'checkout', '-f', '-B', REPOSITORY_BRANCH_NAME, 'origin/%s' % REPOSITORY_BRANCH_NAME]
    else:
        cmd = ['svn', 'switch', REPOSITORY_BRANCH_NAME, '--ignore-ancestry', '--ignore-externals']
    execute_cmd(cmd, cwd_dir, 'switch branch failure!')


def reset_repo_internal(work_dir):
    print('start reset local repository that remote path is \"%s\"' % REPOSITORY_REMOTE_PATH)
    cmd = None
    switch_branch(work_dir)
    if VCS_TYPE == 'GIT':
        cmd = ['git', 'fetch']
        execute_cmd(cmd, work_dir, 'git fetch failure!')
        cmd = ['git', 'reset', '--hard', 'FETCH_HEAD']
        execute_cmd(cmd, work_dir, 'git reset failure!')
        cmd = ['git', 'clean', '-xdf']
        execute_cmd(cmd, work_dir, 'git clean failure!')
    else:
        cmd = ['svn', 'cleanup']
        execute_cmd(cmd, work_dir, 'svn cleanup failure!')
        cmd = ['svn', 'revert', '-R', '.']
        execute_cmd(cmd, work_dir, 'svn reset failure!')
        cmd = ['svn', 'update']
        execute_cmd(cmd, work_dir, 'svn update failure!')
        cmd = ['svn', 'cleanup', '--remove-unversioned']
        execute_cmd(cmd, work_dir, 'svn remove unversioned files failure!')


def clone_or_update_local_repository():
    local_repository_path = pathlib.Path('%s/%s' % (LOCAL_REPOSITORY_PARENT_PATH, LOCAL_REPOSITORY_DIR_NAME))
    if not os.path.exists(local_repository_path.as_posix()):  # clone repository
        clone_repo_internal(local_repository_path.as_posix())
    else:
        reset_repo_internal(local_repository_path.as_posix())


def excute(argv):
    parser_commandline_args(argv)
    clone_or_update_local_repository()


if __name__ == '__main__':
    excute(sys.argv)
