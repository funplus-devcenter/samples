dataform = {}

function dataform:BindView(uiWnd)
	self.uiWnd = uiWnd
	self.uiref = {}
	self.uiref.txt_name = uiWnd.transform:Find("txt_name"):GetComponent("UnityEngine.UI.Text")
	self.uiref.btn_back = uiWnd.transform:Find("btn_back"):GetComponent("UnityEngine.UI.Button")
	self.uiref.btn_back.onClick:AddListener(self.onClick_btn_back)
	self.uiref.ip_id = uiWnd.transform:Find("ip_id"):GetComponent("UnityEngine.UI.InputField")
	self.uiref.btn_query = uiWnd.transform:Find("btn_query"):GetComponent("UnityEngine.UI.Button")
	self.uiref.btn_query.onClick:AddListener(self.onClick_btn_query)
	self.uiWnd.OnInit:AddListener(self.onInit)
	self.uiWnd.OnEnter:AddListener(self.onEnter)
	self.uiWnd.OnExit:AddListener(self.onExit)
	self.uiWnd.OnClose:AddListener(self.onClose)
	self.uiWnd.OnUpdate:AddListener(self.onUpdate)
end

