require 'ui/dataformview'

local this = dataform

function dataform.onClick_btn_back()
	CS.GameLogic.GameEntry.UI:CloseWindow("dataform", true);
end

function dataform.onClick_btn_query()
	local id = tonumber(this.uiref.ip_id.text)
	local config = conf.data:GetAbilityBuffSpec(id);
	if config then
		this.uiref.txt_name.text = config.ControllerClassName;
		print("config.specId:"..config.SpecId..", class:"..config.ControllerClassName);
	else
		this.uiref.txt_name.text = "查询的配置id不存在"
	end
end

function dataform.onInit()
end

function dataform.onEnter()
end

function dataform.onExit()
end

function dataform.onClose()
end

function dataform.onUpdate()
end

