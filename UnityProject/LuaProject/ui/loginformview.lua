loginform = {}

function loginform:BindView(uiWnd)
	self.uiWnd = uiWnd
	self.uiref = {}
	self.uiref.input_user = uiWnd.transform:Find("input_user"):GetComponent("UnityEngine.UI.InputField")
	self.uiref.input_user.onEndEdit:AddListener(self.onEndEdit_user)
	self.uiref.input_password = uiWnd.transform:Find("input_password"):GetComponent("UnityEngine.UI.InputField")
	self.uiref.input_password.onEndEdit:AddListener(self.onEndEdit_password)
	self.uiref.btn_login = uiWnd.transform:Find("btn_login"):GetComponent("UnityEngine.UI.Button")
	self.uiref.btn_login.onClick:AddListener(self.onClick_login)
	self.uiWnd.OnInit:AddListener(self.onInit)
	self.uiWnd.OnEnter:AddListener(self.onEnter)
	self.uiWnd.OnExit:AddListener(self.onExit)
	self.uiWnd.OnClose:AddListener(self.onClose)
	self.uiWnd.OnUpdate:AddListener(self.onUpdate)
end

