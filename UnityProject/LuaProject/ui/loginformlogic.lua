require 'ui/loginformview'

local this = loginform
local network = CS.GameLogic.GameEntry.Network
local ui = CS.GameLogic.GameEntry.UI
local player = playerInfo

function loginform.onEndEdit_user(v)
	this.userName = v;
end

function loginform.onEndEdit_password(v)
	this.pwd = v;
end

function loginform.onClick_login()
	--LuaLogger.LogDebug("login. user:"..this.userName..", password:"..this.pwd);
	network:StartTcpConnect("10.0.84.202", 8990);
end

function loginform.onInit()
end

function loginform.onEnter()
	network:onConnectSuccess('+', this.onConnectSuccess);
	network:onConnectFailed('+', this.onConnectFailed);
	network:onDisconnect('+', this.onDisconnect);
end

function loginform.onExit()
	network:onConnectSuccess('-', this.onConnectSuccess);
	network:onConnectFailed('-', this.onConnectFailed);
	network:onDisconnect('-', this.onDisconnect);
end

function loginform.onClose()
end

function loginform.onUpdate()
end

function loginform.onConnectSuccess()
	LuaLogger.LogDebug("connect success");
	local authorize = msg.Authorize:new();
	authorize.PlayerID = tonumber(this.uiref.input_user.text);
	ProtokitClient.SendMsg(authorize, this.onLogin);
end

function loginform.onConnectFailed()
	LuaLogger.LogDebug("connect failed");
end

function loginform.onDisconnect(reason)
	LuaLogger.LogDebug("onDisconnect, reason:"..reason);
end

function loginform.onLogin(uri, bytes)
	if uri == msg.Authorize:getMessageName() then
		local authorize = msg.Authorize:newFromBytes(bytes);
		player.Id = authorize.PlayerID;
		player.Token = authorize.GameAccessToken;
		ui:OpenWindow("mainform", CS.CenturyGame.Framework.UI.EWindowLayer.Common);
		LuaLogger.LogDebug("onLogin, player"..authorize.PlayerID);
		ui:CloseWindow("loginform", true);
	elseif uri == netutils.ErrorResponse:getMessageName() then
		local errRsp = netutils.ErrorResponse:newFromBytes(bytes);
		warn("request authorize response error. code:"..errRsp.code..", message:"..errRsp.message);
	else
		warn("request authorize response unexcepect proto. uri:"..uri);
	end
end
