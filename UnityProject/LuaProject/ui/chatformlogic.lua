require 'ui/chatformview'

local this = chatform
local network = CS.GameLogic.GameEntry.Network
local http = CS.GameLogic.GameEntry.Http
local player = playerInfo

function chatform.onClick_btn_tcp_chat()
	local chat = this.GetChatMsg(" (lua tcp) ");
	ProtokitClient.SendMsg(chat);
end

function chatform.GetChatMsg(method)
	local chat = msg.Chat:new();
	chat.PlayerID = player.Id;
	chat.text = tostring(chat.PlayerID).." say "..this.uiref.input_chat.text..method;
	return chat;
end

function chatform.onClick_btn_http_post()
	local chat = this.GetChatMsg(" (lua http/post) ");
	ProtokitHttp.PostMsg("http://10.0.84.202:8991/api/v1/chat", chat, function(uri, bytes) 
		if uri == netutils.NormalAck:getMessageName() then
			local NormalAck = netutils.NormalAck:newFromBytes(bytes);
			print("Post Chat Success, response msg:"..NormalAck:getMessageName());
		else
			print("Post Chat Failed");
		end
	end);
end

function chatform.onClick_btn_http_get()
	local chat = this.GetChatMsg(" (lua http/get) ");
	local header = {
		["x-fun-user-token"] = player.Token,
		["charset"] = "utf-8",
	}
	http:GetHttpRequest("http://10.0.84.202:8991/api/v1/chat", header, chat, this.OnHttpChat);
end

function chatform.OnHttpChat(bytes)
end

function chatform.onClick_btn_back()
	CS.GameLogic.GameEntry.UI:CloseWindow("chatform", false);
end

function chatform.onInit()
end

function chatform.onEnter()
	ProtokitClient.RegisterMessageHandler(msg.Chat:getMessageName(), this.OnChat);
	ProtokitHttp.RegisterMessageHandler(netutils.NormalAck:getMessageName(), this.OnHttpNormalAck);
	ProtokitHttpDisorder.RegisterMessageHandler(msg.MockMessageRsp:getMessageName(), this.OnMockResponse);
end

function chatform.onExit()
	ProtokitClient.RemoveMessageHandler(msg.Chat:getMessageName(), this.OnChat);
	ProtokitHttp.RemoveMessageHandler(netutils.NormalAck:getMessageName(), this.OnHttpNormalAck);
	ProtokitHttpDisorder.RemoveMessageHandler(msg.MockMessageRsp:getMessageName(), this.OnMockResponse);
end

function chatform.onClose()
end

function chatform.onUpdate()
end

function chatform.OnChat(bytes)
	local chat = msg.Chat:newFromBytes(bytes);
	this.uiref.txt_content.text = this.uiref.txt_content.text.."\n"..chat.text;
end

function chatform.OnHttpNormalAck(bytes)
	local NormalAck = netutils.NormalAck:newFromBytes(bytes);
	print("OnHttpNormalAck");
end

function chatform.onClick_btn_clear()
	this.uiref.txt_content.text = "";
end

function chatform.onClick_btn_http_disorder()
	local mock = msg.MockMessageReq:new();
	mock.NetScheme = "HttpClientLuaDisorder";
	mock.Type = msg.MockType_MockNormal;
	--for i = 1, 10 do
		ProtokitHttpDisorder.PostMsg("http://test-ALB-650999881.us-west-2.elb.amazonaws.com:9991/api/v1/Mock", mock);
	--end
end

function chatform.OnMockResponse(bytes)
	local mockRsp = msg.MockMessageRsp:newFromBytes(bytes);
	print("Recv MockResponse:"..mockRsp.NetScheme);
end

