chatform = {}

function chatform:BindView(uiWnd)
	self.uiWnd = uiWnd
	self.uiref = {}
	self.uiref.txt_content = uiWnd.transform:Find("txt_content"):GetComponent("UnityEngine.UI.Text")
	self.uiref.btn_tcp_chat = uiWnd.transform:Find("btn_tcp_chat"):GetComponent("UnityEngine.UI.Button")
	self.uiref.btn_tcp_chat.onClick:AddListener(self.onClick_btn_tcp_chat)
	self.uiref.input_chat = uiWnd.transform:Find("input_chat"):GetComponent("UnityEngine.UI.InputField")
	self.uiref.btn_http_post = uiWnd.transform:Find("btn_http_post"):GetComponent("UnityEngine.UI.Button")
	self.uiref.btn_http_post.onClick:AddListener(self.onClick_btn_http_post)
	self.uiref.btn_http_get = uiWnd.transform:Find("btn_http_get"):GetComponent("UnityEngine.UI.Button")
	self.uiref.btn_http_get.onClick:AddListener(self.onClick_btn_http_get)
	self.uiref.btn_back = uiWnd.transform:Find("btn_back"):GetComponent("UnityEngine.UI.Button")
	self.uiref.btn_back.onClick:AddListener(self.onClick_btn_back)
	self.uiref.btn_clear = uiWnd.transform:Find("btn_clear"):GetComponent("UnityEngine.UI.Button")
	self.uiref.btn_clear.onClick:AddListener(self.onClick_btn_clear)
	self.uiref.btn_http_disorder = uiWnd.transform:Find("btn_http_disorder"):GetComponent("UnityEngine.UI.Button")
	self.uiref.btn_http_disorder.onClick:AddListener(self.onClick_btn_http_disorder)
	self.uiWnd.OnInit:AddListener(self.onInit)
	self.uiWnd.OnEnter:AddListener(self.onEnter)
	self.uiWnd.OnExit:AddListener(self.onExit)
	self.uiWnd.OnClose:AddListener(self.onClose)
	self.uiWnd.OnUpdate:AddListener(self.onUpdate)
end

