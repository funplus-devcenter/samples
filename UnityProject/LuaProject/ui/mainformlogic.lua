require 'ui/mainformview'

local this = mainform
local ui = CS.GameLogic.GameEntry.UI

function mainform.onClick_Btn_chat()
	ui:OpenWindow("chatform", CS.CenturyGame.Framework.UI.EWindowLayer.Pop);
end

function mainform.onClick_Btn_config()
	ui:OpenWindow("dataform", CS.CenturyGame.Framework.UI.EWindowLayer.Pop);
end

function mainform.onInit()
end

function mainform.onEnter()
end

function mainform.onExit()
end

function mainform.onClose()
end

function mainform.onUpdate()
end

