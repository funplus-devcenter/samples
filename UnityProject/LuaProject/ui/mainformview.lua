mainform = {}

function mainform:BindView(uiWnd)
	self.uiWnd = uiWnd
	self.uiref = {}
	self.uiref.Btn_chat = uiWnd.transform:Find("Btn_chat"):GetComponent("UnityEngine.UI.Button")
	self.uiref.Btn_chat.onClick:AddListener(self.onClick_Btn_chat)
	self.uiref.Btn_config = uiWnd.transform:Find("Btn_config"):GetComponent("UnityEngine.UI.Button")
	self.uiref.Btn_config.onClick:AddListener(self.onClick_Btn_config)
	self.uiWnd.OnInit:AddListener(self.onInit)
	self.uiWnd.OnEnter:AddListener(self.onEnter)
	self.uiWnd.OnExit:AddListener(self.onExit)
	self.uiWnd.OnClose:AddListener(self.onClose)
	self.uiWnd.OnUpdate:AddListener(self.onUpdate)
end

