--C#全局变量
declare("NetworkTrace", CS.CenturyGame.Framework.Network.Trace.Instance);
declare("LuaLogger", CS.CenturyGame.Framework.Lua.LuaLogger);

--lua全局变量
declare("ProtokitClient", require("protokithelp.protokitclient"));
declare("ProtokitHttp", require("protokithelp.protokithttp"));
declare("ProtokitHttpDisorder", require("protokithelp.protokithttpdisorder"));
