-- Code generated by ProtoKitGo. DO NOT EDIT.
-- input: service/chat_service.proto

local pb = require "pb"
local protoc = require "protoc"
require(GEN_PACKAGE_NAME .. ".msg.chat_pb")
require(GEN_PACKAGE_NAME .. ".netutils.common_pb")

local ChatService = {}

ChatService.Chat = {}
ChatService.Chat.uri = "/api/v1/chat"
ChatService.Chat.req = msg.Chat
ChatService.Chat.rsp = netutils.NormalAck

service.ChatService = ChatService

