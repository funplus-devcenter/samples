-- Code generated by ProtoKitGo. DO NOT EDIT.
-- input: msg/chat.proto

local pb = require "pb"
local protoc = require "protoc"
local protoStr = [==[
syntax = "proto3";
package msg;
option go_package = "bitbucket.org/funplus/sandwich-sample-conf/gen/golang/msg";
option csharp_namespace = "gen.msg";
message Chat {
    uint64 PlayerID = 100;
    string text = 1;
    repeated int32 test =2;
}


]==]
assert(protoc:load(protoStr,"msg/chat.proto"))

---@class Chat
---@field public PlayerID number
---@field public text string
---@field public test number[]
local Chat={}
Chat.__index = Chat
---@return Chat
function Chat:new(data) return setmetatable(data or {},Chat)  end
---@return Chat
function Chat:newFromBytes(bytes) return setmetatable(pb.decode(self:getMessageName(),bytes) or {},Chat)  end
---@return string
function Chat:getMessageName() return "msg.Chat" end
---@return string
function Chat:marshal()  return pb.encode(self:getMessageName(),self) end
msg.Chat = Chat

