return {
    ["10001_name1"] = {
        ["id"] = 10001,
        ["name"] = "name1",
        ["bags"] = {
            ["10000"] = 5,
        },
        ["jsontest"] = [==[{"array":[1,2,3],"boolean":true,"null":null,"number":123,"object":{"a":"b","c":"d","e":"f"},"string":"Hello World"}]==],
        ["skill_level"] = {},
        ["testbool"] = true,
        ["testfloat"] = 111.11,
        ["testint32"] = 2,
    },
}
