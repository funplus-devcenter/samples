local protokithttp = {}

local msgQueue = {};
local handlerQueue = {};

local messageHandler = {};
local reqeustHandler = {};
local batchRequests = {};

local isInit = false;

---@param MsgBatchLimit number @Queue模式最大消息合并数量 
local MsgBatchLimit = 10;

local HttpClient;

---@param EnableBatch boolean @是否使用Queue模式合并消息 
local EnableBatch = false;

---@param AutoRetry boolean @请求失败后是否自动重试
local AutoRetry = true;

function protokithttp.Init()
    if not isInit then
        HttpClient = CS.ProtokitHelper.ProtokitHttpClient.Instance;
        HttpClient:Init(EnableBatch, AutoRetry, MsgBatchLimit);
        HttpClient:Evt_RecvMsg('+', protokithttp.RecvMsg);
        HttpClient:Evt_RecvPackFinish('+', protokithttp.FinishRecv);
        HttpClient:Evt_Update('+', protokithttp.Update);
        isInit = true;
        --应该由业务层在合适的地方监听下面的事件
        --请求返回错误状态
        -- HttpClient:Evt_RspFailed('+', protokithttp.ResponseFailed);
        --单次请求开始
        -- HttpClient:Evt_ReqBegin('+', protokithttp.RequestBegin);
        --单次请求结束
        -- HttpClient:Evt_ReqEnd('+', protokithttp.RequestEnd);
        --请求达到重试次数上限仍未成功
        -- HttpClient:Evt_ReqFailed('+', protokithttp.RequestFailed);
        --超出预期请求时间
        -- HttpClient:Evt_ReqTimeOutExpect('+', protokithttp.RequestTimeOutExpect);
    end
end

function protokithttp.PostMsg(url, msg, handler)
    if msg.getMessageName then
        local index = #msgQueue + 1;
        msgQueue[index] = {};
        msgQueue[index].url = url;
        msgQueue[index].msg = msg;
        if handler ~= nil then
            handlerQueue[index] = handler;
        end
    end
end

function protokithttp.Update()
    if EnableBatch then
        if #msgQueue > 0 then
            local sendIndex = 1;
            while sendIndex <= #msgQueue do
                local sendMsgs = {};
                local handlers = {};
                local httpUrl;
                while #sendMsgs < MsgBatchLimit and sendIndex <= #msgQueue do
                    local index = #sendMsgs + 1;
                    if index == 1 then
                        httpUrl = msgQueue[sendIndex].url;
                    else
                        if msgQueue[sendIndex].url ~= httpUrl then
                            break;
                        end
                    end
                    table.insert(sendMsgs, index, msgQueue[sendIndex].msg);
                    if handlerQueue[sendIndex] ~= nil then
                        handlers[index] = handlerQueue[sendIndex];
                    end
                    sendIndex = sendIndex + 1;
                end
                local packet, bytes = netutils:marshalRawPacket(sendMsgs);
                batchRequests[packet.SequenceID] = {};
                for k, v in ipairs(packet.rawAny) do
                    if handlers[k] ~= nil then
                        reqeustHandler[v.passThrough] = handlers[k];
                        local tbl = {};
                        tbl.passThrough = v.passThrough;
                        tbl.uri = v.uri;
                        table.insert(batchRequests[packet.SequenceID], tbl);
                    end
                    NetworkTrace:debug(string.format("[HTTP][Send] %s", v.uri));
                end
                HttpClient:PostRequestBytes(httpUrl, bytes);
            end
            msgQueue = {};
            handlerQueue = {};
        end
    else
        if #msgQueue > 0 then
            for i = 1, #msgQueue do
                local req = msgQueue[i];
                local packet, bytes = netutils:marshalRawPacket(req.msg);
                batchRequests[packet.SequenceID] = {};
                for k, v in ipairs(packet.rawAny) do
                    if handlerQueue[i] ~= nil then
                        reqeustHandler[v.passThrough] = handlerQueue[i];
                        local tbl = {};
                        tbl.passThrough = v.passThrough;
                        tbl.uri = v.uri;
                        table.insert(batchRequests[packet.SequenceID], tbl);
                    end
                    NetworkTrace:debug(string.format("[HTTP][Send] %s", v.uri));
                end
                HttpClient:PostRequestBytes(req.url, bytes);
            end
            msgQueue = {};
            handlerQueue = {};
        end
    end
end

---@type fun(uri:string, handler:function) @注册对某一类型消息的监听函数
---@param uri string @消息名称
---@param handler function @消息处理回调
function protokithttp.RegisterMessageHandler(uri, handler)
    if handler == nil then
        return
    end
    if messageHandler[uri] == nil then
        messageHandler[uri] = {};
    end
    table.insert(messageHandler[uri], handler);
end

---@type fun(uri:string, handler:function) @移除对某一类型消息的监听函数
---@param uri string @消息名称
---@param handler function @消息处理回调
function protokithttp.RemoveMessageHandler(uri, handler)
    if handler == nil then
        return
    end
    if messageHandler[uri] ~= nil then
        local index = 0;
        for i = 1, #messageHandler[uri] do
            if messageHandler[uri][i] == handler then
                index = i;
                break;
            end
        end
        if index ~= 0 then
            table.remove(messageHandler[uri], index);
        end
    end
end

function protokithttp.RecvMsg(sequenceId, passThrough, uri, bytes)
    if messageHandler[uri] ~= nil then
        for k,v in pairs(messageHandler[uri]) do
            v(bytes);
        end
    end
    if reqeustHandler[passThrough] ~= nil then
        reqeustHandler[passThrough](uri, bytes);
        reqeustHandler[passThrough] = nil;
    end
    if uri == netutils.ErrorResponse:getMessageName() then
        local errorMsg = netutils.ErrorResponse:newFromBytes(bytes);
        if not errorMsg.LogicException then
            protokithttp.RecvCommonError(sequenceId, passThrough);
        end
    end
end

function protokithttp.RecvCommonError(sequenceId, passThrough)
    if batchRequests[sequenceId] ~= nil then
        for k, v in pairs(batchRequests[sequenceId]) do
            if reqeustHandler[v.passThrough] ~= nil then
                reqeustHandler[v.passThrough] = nil;
            end
            if v.passThrough == passThrough then
                NetworkTrace:warn("Common error caused by "..v.uri);
            end
        end
        batchRequests[sequenceId] = nil;
    end
end

function protokithttp.FinishRecv(sequenceId)
    if batchRequests[sequenceId] ~= nil then
        for k, v in pairs(batchRequests[sequenceId]) do
            if reqeustHandler[v.passThrough] ~= nil then
                reqeustHandler[v.passThrough] = nil;
            end
        end
        batchRequests[sequenceId] = nil;
    end
end

function protokithttp.ResponseFailed(statusCode, statusName)
    --Http返回错误状态
end

function protokithttp.RequestBegin()
    --单次请求开始
    print("RequestBegin");
end

function protokithttp.RequestEnd()
    --单次请求结束
    print("RequestEnd");
end

function protokithttp.RequestFailed()
    --请求达到重试次数上限仍未成功，可弹窗提示用户确认网络状况后手动重试(调用HttpClient:Retry)，或者停止HttpClient的所有请求(调用HttpClient:Stop)后退回登录界面
    print("RequestFailed");
end

function protokithttp.RequestTimeOutExpect()
    --超出预期请求时间，可能需要显示转圈界面，当请求结束时关闭转圈界面
    print("RequestTimeOutExpect");
end

return protokithttp;