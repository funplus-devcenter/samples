local _require = require
_G["require"] = function(modulename, ...)
    modulename = string.gsub(modulename, "/", ".")
    return _require(modulename, ...);
end

playerInfo = {}
require 'gen/init'
gen:initAll()

require 'luaframe.frame'
require 'globaldefine'

for key,loader in pairs(conf.data.loaders) do 
	LuaLogger.LogDebug("load config "..key)
    luaName = key .. ".lua"
    loader(luaName,require ("data."..key))
end

data_table_accessor = require('data_table_accessor')
