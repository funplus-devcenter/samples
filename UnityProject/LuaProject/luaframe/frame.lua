local _G = _G
local rawset = rawset

function declare(name, value)
    if string.match(name, "^__%u[%u%d_]*$") then
        error("attempt to declare variable: " .. name, 2)
        return
    end
    -- __spark_reload_global[name] = true
    rawset(_G, name, value)
end

require 'luaframe.function'
require 'luaframe.class'