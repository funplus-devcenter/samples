local function class()
    local c = {};
    c.__index = c;
    c.__newindex = function(t,k,v)
        rawset(t,k,v)
    end

    local mt = {};
    mt.__call = function(ClassTbl, ...)
        local instance = {};
        setmetatable(instance, ClassTbl);
        local ctor = ClassTbl.ctor;
        if ctor then
            ctor(instance, ...);
        end
        return instance;
    end

    setmetatable(c, mt);
    return c;
end

declare("class", class);