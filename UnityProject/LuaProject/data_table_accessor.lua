local accessor = {}
local loader = require('gen.conf.gen_load') 

function accessor.GetItem(table_name , id)
    if table_name == nil or id == nil then
        return nil
    end
    if type(table_name) ~= 'string' then
        return nil
    end
    print(type(id))
    local get_func = loader["Get"..table_name]
    return get_func(loader,id)
end

return accessor