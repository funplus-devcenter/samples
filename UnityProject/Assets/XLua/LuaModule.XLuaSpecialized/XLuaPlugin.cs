/***************************************************************

 *  类名称：        XLuaPlugin

 *  描述：				

 *  作者：          Chico(wuyuanbing)

 *  创建时间：      2020/6/1 20:58:11

 *  最后修改人：

 *  版权所有 （C）:   diandiangames

***************************************************************/

using System.IO;
using CenturyGame.AppUpdaterLib.Runtime;
using CenturyGame.LuaModule.Runtime;
using CenturyGame.LuaModule.Runtime.Interfaces;
using UnityEngine;
//using UnityEngine.Networking;
using XLua;

namespace CenturyGame.LuaModule.XLuaSpecialized
{
    public sealed class XLuaPlugin : ILuaPlugin
    {
        //--------------------------------------------------------------
        #region Fields
        //--------------------------------------------------------------

        private ILuaEnvironment mLuaEnv;

        #endregion

        //--------------------------------------------------------------
        #region Properties & Events
        //--------------------------------------------------------------

        #endregion

        //--------------------------------------------------------------
        #region Creation & Cleanup
        //--------------------------------------------------------------

        #endregion

        //--------------------------------------------------------------
        #region Methods
        //--------------------------------------------------------------

        #endregion

        public string Name { get; } = "XLua";

        private AssetBundle mluaAssetBundle;

        public void OnInit()
        {
            LuaEnv luaEnv = new LuaEnv();
            luaEnv.AddLoader(this.CustomLuaLoad);
            this.mLuaEnv = new XLuaEnv(luaEnv);
            LuaAccessManager.Init(this.mLuaEnv);
        }

        public void Tick()
        {
            this.mLuaEnv.Tick();
        }

        private byte[] CustomLuaLoad(ref string fileName)
        {
#if UNITY_EDITOR
            if (AssetBundleManager.Runtime.AssetBundleManager.SimulateAssetBundleInEditor ||
                (!AssetBundleManager.Runtime.AssetBundleManager.SimulateAssetBundleInEditor && LuaDevelopmentMode.LuaDevelopmentModeInEditor))
            {
                int rootEndIndex = Application.dataPath.LastIndexOf('/');
                string luaRoot = Application.dataPath.Substring(0, rootEndIndex);
                string filePath = fileName.Replace('.', '/');
                string fullPath = string.Concat(luaRoot, "/LuaProject/", filePath, ".lua");
                if (File.Exists(fullPath))
                    return File.ReadAllBytes(fullPath);
                return null;
            }
            else
            {
                string luafilePath = AssetsFileSystem.GetWritePath($"{fileName.Replace('.', '/')}.lua");
                if (File.Exists(luafilePath))
                {
                    return File.ReadAllBytes(luafilePath);
                }
                else
                {
                    if (this.mluaAssetBundle == null)
                    {
                        string abPath = AssetsFileSystem.GetWritePath($"lua.x");

                        if (!File.Exists(abPath))
                        {
#if APPEND_PLATFORM_NAME
                            abPath = $"{Application.streamingAssetsPath}/{Utility.GetPlatformName()}/lua.x";
#else
                            abPath = $"{Application.streamingAssetsPath}/lua.x";
#endif
                        }
                        this.mluaAssetBundle = AssetBundle.LoadFromFile(abPath);
                    }
                    var path = $"lua/{fileName.Replace('.', '/')}";
                    var textAssets = this.mluaAssetBundle.LoadAsset<TextAsset>($"Assets/{path}.bytes");
                    var bytes = textAssets.bytes;
                    return bytes;
                }
            }
#else
            string luafilePath = AssetsFileSystem.GetWritePath($"{fileName.Replace('.', '/')}.lua");
            if (File.Exists(luafilePath))
            {
                return File.ReadAllBytes(luafilePath);
            }
            else
            {
                if (this.mluaAssetBundle == null)
                {
                    string abPath = AssetsFileSystem.GetWritePath("lua.x");

                    if (!File.Exists(abPath))
                    {
#if APPEND_PLATFORM_NAME
                        abPath = $"{Application.streamingAssetsPath}/{Utility.GetPlatformName()}/lua.x";
#else
                        abPath = $"{Application.streamingAssetsPath}/lua.x";
#endif
                    }
                    this.mluaAssetBundle = AssetBundle.LoadFromFile(abPath);
                }
                var path = $"lua/{fileName.Replace('.', '/')}";
                var textAssets = this.mluaAssetBundle.LoadAsset<TextAsset>($"Assets/{path}.bytes");
                var bytes = textAssets.bytes;
                return bytes;
            }
            
#endif
        }

        public void OnDispose()
        {
            this.mLuaEnv?.Dispose();
        }
    }
}
