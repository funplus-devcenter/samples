﻿#if UNITY_EDITOR

using UnityEditor;

public static class LuaDevelopmentMode
{
    const string kLuaDevelopmentMode = "LuaDevelopmentMode";
    public static bool LuaDevelopmentModeInEditor
    {
        get
        {
            if (m_LuaDevelopmentMode == -1)
                m_LuaDevelopmentMode = EditorPrefs.GetBool(kLuaDevelopmentMode, false) ? 1 : 0;

            return m_LuaDevelopmentMode != 0;
        }
        set
        {
            int newValue = value ? 1 : 0;
            if (newValue != m_LuaDevelopmentMode)
            {
                m_LuaDevelopmentMode = newValue;
                EditorPrefs.SetBool(kLuaDevelopmentMode, value);
            }
        }
    }
    static int m_LuaDevelopmentMode = -1;
}

#endif