﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CenturyGame.LuaModule.XLuaSpecialized.Editor
{
    static class XLuaMenus
    {
        const string kLuaDevelopmentMode = "CenturyGame/AssetBundleManager/Lua Development Mode";

        [MenuItem(kLuaDevelopmentMode)]
        public static void ToggleLuaDevelopmentMode()
        {
            LuaDevelopmentMode.LuaDevelopmentModeInEditor = !LuaDevelopmentMode.LuaDevelopmentModeInEditor;
        }

        [MenuItem(kLuaDevelopmentMode, true,priority = 1000)]
        public static bool ToggleLuaDevelopmentModeValidate()
        {
            Menu.SetChecked(kLuaDevelopmentMode, LuaDevelopmentMode.LuaDevelopmentModeInEditor);
            return true;
        }
    }
}

