/***************************************************************

 *  类名称：        XLuaEnv

 *  描述：				

 *  作者：          Chico(wuyuanbing)

 *  创建时间：      2020/6/1 12:20:55

 *  最后修改人：

 *  版权所有 （C）:   diandiangames

***************************************************************/


using CenturyGame.LuaModule.Runtime.Interfaces;
using System.Net.NetworkInformation;
using XLua;

namespace CenturyGame.LuaModule.XLuaSpecialized
{
    public sealed class XLuaEnv : ILuaEnvironment
    {
        //--------------------------------------------------------------
        #region Fields
        //--------------------------------------------------------------

        private LuaEnv mEnv;

        private ILuaTable mGlobalTable;

        #endregion

        //--------------------------------------------------------------
        #region Properties & Events
        //--------------------------------------------------------------

        #endregion

        //--------------------------------------------------------------
        #region Creation & Cleanup
        //--------------------------------------------------------------

        public XLuaEnv(LuaEnv env)
        {
            this.mEnv = env;

            this.mGlobalTable = new XLuaTableWraper(this.mEnv.Global);
        }

        #endregion

        //--------------------------------------------------------------
        #region Methods
        //--------------------------------------------------------------



        #endregion

        public void Dispose()
        {
            this.mEnv?.Dispose();
        }

        public ILuaTable Global => this.mGlobalTable;

        public object[] DoString(byte[] chunk, string chunkName = "chunk")
        {
            return this.mEnv.DoString(chunk, chunkName);
        }

        public object[] DoString(string chunk, string chunkName = "chunk")
        {
            return this.mEnv.DoString(chunk,chunkName);
        }

        public void Tick()
        {
            this.mEnv?.Tick();
        }

        public void GC()
        {
            this.mEnv?.GC();
        }

        public ILuaTable NewTable()
        {
            var xluaTable = this.mEnv.NewTable();

            return new XLuaTableWraper(xluaTable);
        }
    }
}
