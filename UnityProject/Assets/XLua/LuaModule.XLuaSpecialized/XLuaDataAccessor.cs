/***************************************************************

 *  类名称：        XLuaDataAccessor

 *  描述：				

 *  作者：          Chico(wuyuanbing)

 *  创建时间：      2021/1/19 14:55:57

 *  最后修改人：

 *  版权所有 （C）:   CenturyGames

***************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CenturyGame.LuaModule.Runtime;
using CenturyGame.LuaModule.Runtime.Interfaces;

namespace XLua.LuaModule.XLuaSpecialized
{
    public static class XLuaDataAccessor
    {
        //--------------------------------------------------------------
        #region Fields
        //--------------------------------------------------------------

        private static ILuaFunction mGetTableDataFunc = null;

        #endregion

        //--------------------------------------------------------------
        #region Properties & Events
        //--------------------------------------------------------------

        #endregion

        //--------------------------------------------------------------
        #region Creation & Cleanup
        //--------------------------------------------------------------

        #endregion

        //--------------------------------------------------------------
        #region Methods
        //--------------------------------------------------------------


        public static LuaTable GetData(string tableName,object id)
        {
            if (mGetTableDataFunc == null)
            {
                var global = LuaAccessManager.Environment.Global;
                var accessor = global.GetTable("data_table_accessor");
                mGetTableDataFunc = accessor.GetFunction("GetItem");
            }
            return (LuaTable)mGetTableDataFunc.Call(tableName,id)[0];
        }

        #endregion

    }
}
