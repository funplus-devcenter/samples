/***************************************************************

 *  类名称：        XLuaTableWraper

 *  描述：				

 *  作者：          Chico(wuyuanbing)

 *  创建时间：      2020/6/1 12:13:04

 *  最后修改人：

 *  版权所有 （C）:   diandiangames

***************************************************************/


using CenturyGame.LuaModule.Runtime.Interfaces;
using System;
using System.Collections;
using XLua;

namespace CenturyGame.LuaModule.XLuaSpecialized
{
    public class XLuaTableWraper : ILuaTable
    {
        //--------------------------------------------------------------
        #region Fields
        //--------------------------------------------------------------

        private LuaTable mTable;

        #endregion

        //--------------------------------------------------------------
        #region Properties & Events
        //--------------------------------------------------------------

        #endregion

        //--------------------------------------------------------------
        #region Creation & Cleanup
        //--------------------------------------------------------------

        public XLuaTableWraper(LuaTable table)
        {
            this.mTable = table;
        }

        #endregion

        //--------------------------------------------------------------
        #region Methods
        //--------------------------------------------------------------

        public void Dispose()
        {
            this.mTable?.Dispose();
        }

        public ILuaTable GetTable(string key)
        {
            this.CheckTable("GetTable(string key)");
            LuaTable xluaTable;
            this.mTable.Get(key, out xluaTable);

            if (xluaTable == null)
                throw new ArgumentException(nameof(key));
            return new XLuaTableWraper(xluaTable);
        }

        public void GetTable(string key, out ILuaTable value)
        {
            this.CheckTable("GetTable(string key, out ILuaTable value)");
            LuaTable xluaTable;
            this.mTable.Get(key, out xluaTable);

            if (xluaTable == null)
                throw new ArgumentException(nameof(key));
            value = new XLuaTableWraper(xluaTable);
        }

        public ILuaFunction GetFunction(string key)
        {
            this.CheckTable("GetFunction(string key)");
            LuaFunction xluaFunc;
            this.mTable.Get(key, out xluaFunc);

            if (xluaFunc == null)
                throw new ArgumentException(nameof(key));
            return new XLuaFunctionWraper(xluaFunc,this.mTable);
        }

        public void GetFunction(string key, out ILuaFunction value)
        {
            this.CheckTable("GetFunction(string key, out ILuaFunction value)");

            LuaFunction xluaFunc;
            this.mTable.Get(key, out xluaFunc);

            if (xluaFunc == null)
                throw new ArgumentException(nameof(key));
            value = new XLuaFunctionWraper(xluaFunc,this.mTable);
        }

        public T GetVal<T>(string key)
        {
            this.CheckTable("GetVal<T>(string key)");

            T val = default(T);
            mTable.Get(key, out val);

            return val;
        }

        public void GetVal<T>(string key, out T value)
        {
            this.CheckTable("GetVal<T>(string key, out T value)");
            value = default(T);
            mTable.Get(key, out value);
        }

        public IEnumerable GetAllKeys()
        {
            this.CheckTable("GetAllKeys");
            return mTable.GetKeys();
        }

        public void SetVal<T>(string key, T value)
        {
            this.CheckTable("SetVal<T>(string key, T value)");
            mTable.Set(key,value);
        }

        public void SetMetaTable(ILuaTable metaTable)
        {
            this.CheckTable("SetMetaTable(ILuaTable metaTable)");
            var xluMetaTable = metaTable as XLuaTableWraper;
            CheckTable(xluMetaTable,"SetMetaTable(ILuaTable metaTable) static check xluTable");
            
            mTable.SetMetaTable(xluMetaTable.mTable);
        }


        private void CheckTable(string methodName)
        {
            if (mTable == null)
                throw new NullReferenceException($"The current xlua table is null ,current function name is \"{methodName}\" .");
        }

        private static void CheckTable(XLuaTableWraper xluTable, string methodName)
        {
            if (xluTable.mTable == null)
                throw new NullReferenceException($"The xluaTable is invalid,current function name is \"{methodName}\" .");
        }

        #endregion


    }
}
