/***************************************************************

 *  类名称：        XLuaFunctionWraper

 *  描述：				

 *  作者：          Chico(wuyuanbing)

 *  创建时间：      2020/6/1 12:14:22

 *  最后修改人：

 *  版权所有 （C）:   diandiangames

***************************************************************/

using CenturyGame.LuaModule.Runtime.Interfaces;
using System;
using XLua;

namespace CenturyGame.LuaModule.XLuaSpecialized
{
    public sealed class XLuaFunctionWraper : ILuaFunction
    {
        private LuaFunction mFunc;
        private LuaTable mTable;


        public XLuaFunctionWraper(LuaFunction func , LuaTable table = null)
        {
            mFunc = func;
            mTable = table;
        }

        public void Dispose()
        {
            mFunc?.Dispose();
        }

        public object[] Call(params object[] args)
        {
            return mFunc?.Call(args);
        }

        public object[] InstCall(params object[] args)
        {
            int parametersLength = (args != null && args.Length > 0) ? args.Length + 1 : 1;

            object[] _args = new object[parametersLength];
            _args[0] = this.mTable;
            if (args != null && args.Length > 0)
                Array.Copy(args,0,_args,1,args.Length);

            return Call(_args);
        }
    }
}
