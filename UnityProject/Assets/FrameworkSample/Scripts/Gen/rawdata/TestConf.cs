// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: rawdata_exclude/TestConf.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace gen.rawdata {

  /// <summary>Holder for reflection information generated from rawdata_exclude/TestConf.proto</summary>
  public static partial class TestConfReflection {

    #region Descriptor
    /// <summary>File descriptor for rawdata_exclude/TestConf.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static TestConfReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "Ch5yYXdkYXRhX2V4Y2x1ZGUvVGVzdENvbmYucHJvdG8SB3Jhd2RhdGEi0wEK",
            "BFRlc3QSCgoCaWQYASABKAUSDAoEbmFtZRgCIAEoCRIlCgRiYWdzGAMgAygL",
            "MhcucmF3ZGF0YS5UZXN0LkJhZ3NFbnRyeRIQCghqc29udGVzdBgEIAEoCRIT",
            "Cgtza2lsbF9sZXZlbBgFIAMoBRIQCgh0ZXN0Ym9vbBgGIAEoCBIRCgl0ZXN0",
            "ZmxvYXQYByABKAISEQoJdGVzdGludDMyGAggASgFGisKCUJhZ3NFbnRyeRIL",
            "CgNrZXkYASABKAkSDQoFdmFsdWUYAiABKAU6AjgBInoKCFRlc3RDb25mEi8K",
            "B1Rlc3RNYXAYASADKAsyHi5yYXdkYXRhLlRlc3RDb25mLlRlc3RNYXBFbnRy",
            "eRo9CgxUZXN0TWFwRW50cnkSCwoDa2V5GAEgASgJEhwKBXZhbHVlGAIgASgL",
            "Mg0ucmF3ZGF0YS5UZXN0OgI4AUJNWj1iaXRidWNrZXQub3JnL2Z1bnBsdXMv",
            "c2FuZHdpY2gtc2FtcGxlLWNvbmYvZ2VuL2dvbGFuZy9yYXdkYXRhqgILZ2Vu",
            "LnJhd2RhdGFiBnByb3RvMw=="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { },
          new pbr::GeneratedClrTypeInfo(null, null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::gen.rawdata.Test), global::gen.rawdata.Test.Parser, new[]{ "Id", "Name", "Bags", "Jsontest", "SkillLevel", "Testbool", "Testfloat", "Testint32" }, null, null, null, new pbr::GeneratedClrTypeInfo[] { null, }),
            new pbr::GeneratedClrTypeInfo(typeof(global::gen.rawdata.TestConf), global::gen.rawdata.TestConf.Parser, new[]{ "TestMap" }, null, null, null, new pbr::GeneratedClrTypeInfo[] { null, })
          }));
    }
    #endregion

  }
  #region Messages
  public sealed partial class Test : pb::IMessage<Test> {
    private static readonly pb::MessageParser<Test> _parser = new pb::MessageParser<Test>(() => new Test());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<Test> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::gen.rawdata.TestConfReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Test() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Test(Test other) : this() {
      id_ = other.id_;
      name_ = other.name_;
      bags_ = other.bags_.Clone();
      jsontest_ = other.jsontest_;
      skillLevel_ = other.skillLevel_.Clone();
      testbool_ = other.testbool_;
      testfloat_ = other.testfloat_;
      testint32_ = other.testint32_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Test Clone() {
      return new Test(this);
    }

    /// <summary>Field number for the "id" field.</summary>
    public const int IdFieldNumber = 1;
    private int id_;
    /// <summary>
    /// commentid
    /// </summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int Id {
      get { return id_; }
      set {
        id_ = value;
      }
    }

    /// <summary>Field number for the "name" field.</summary>
    public const int NameFieldNumber = 2;
    private string name_ = "";
    /// <summary>
    /// commentname
    /// </summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public string Name {
      get { return name_; }
      set {
        name_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }

    /// <summary>Field number for the "bags" field.</summary>
    public const int BagsFieldNumber = 3;
    private static readonly pbc::MapField<string, int>.Codec _map_bags_codec
        = new pbc::MapField<string, int>.Codec(pb::FieldCodec.ForString(10, ""), pb::FieldCodec.ForInt32(16, 0), 26);
    private readonly pbc::MapField<string, int> bags_ = new pbc::MapField<string, int>();
    /// <summary>
    /// testmap
    /// map
    /// 1111
    /// </summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public pbc::MapField<string, int> Bags {
      get { return bags_; }
    }

    /// <summary>Field number for the "jsontest" field.</summary>
    public const int JsontestFieldNumber = 4;
    private string jsontest_ = "";
    /// <summary>
    /// json数据 with format
    /// </summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public string Jsontest {
      get { return jsontest_; }
      set {
        jsontest_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }

    /// <summary>Field number for the "skill_level" field.</summary>
    public const int SkillLevelFieldNumber = 5;
    private static readonly pb::FieldCodec<int> _repeated_skillLevel_codec
        = pb::FieldCodec.ForInt32(42);
    private readonly pbc::RepeatedField<int> skillLevel_ = new pbc::RepeatedField<int>();
    /// <summary>
    /// testrepeated
    /// </summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public pbc::RepeatedField<int> SkillLevel {
      get { return skillLevel_; }
    }

    /// <summary>Field number for the "testbool" field.</summary>
    public const int TestboolFieldNumber = 6;
    private bool testbool_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Testbool {
      get { return testbool_; }
      set {
        testbool_ = value;
      }
    }

    /// <summary>Field number for the "testfloat" field.</summary>
    public const int TestfloatFieldNumber = 7;
    private float testfloat_;
    /// <summary>
    /// 中文
    /// </summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public float Testfloat {
      get { return testfloat_; }
      set {
        testfloat_ = value;
      }
    }

    /// <summary>Field number for the "testint32" field.</summary>
    public const int Testint32FieldNumber = 8;
    private int testint32_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int Testint32 {
      get { return testint32_; }
      set {
        testint32_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as Test);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(Test other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (Id != other.Id) return false;
      if (Name != other.Name) return false;
      if (!Bags.Equals(other.Bags)) return false;
      if (Jsontest != other.Jsontest) return false;
      if(!skillLevel_.Equals(other.skillLevel_)) return false;
      if (Testbool != other.Testbool) return false;
      if (!pbc::ProtobufEqualityComparers.BitwiseSingleEqualityComparer.Equals(Testfloat, other.Testfloat)) return false;
      if (Testint32 != other.Testint32) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (Id != 0) hash ^= Id.GetHashCode();
      if (Name.Length != 0) hash ^= Name.GetHashCode();
      hash ^= Bags.GetHashCode();
      if (Jsontest.Length != 0) hash ^= Jsontest.GetHashCode();
      hash ^= skillLevel_.GetHashCode();
      if (Testbool != false) hash ^= Testbool.GetHashCode();
      if (Testfloat != 0F) hash ^= pbc::ProtobufEqualityComparers.BitwiseSingleEqualityComparer.GetHashCode(Testfloat);
      if (Testint32 != 0) hash ^= Testint32.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (Id != 0) {
        output.WriteRawTag(8);
        output.WriteInt32(Id);
      }
      if (Name.Length != 0) {
        output.WriteRawTag(18);
        output.WriteString(Name);
      }
      bags_.WriteTo(output, _map_bags_codec);
      if (Jsontest.Length != 0) {
        output.WriteRawTag(34);
        output.WriteString(Jsontest);
      }
      skillLevel_.WriteTo(output, _repeated_skillLevel_codec);
      if (Testbool != false) {
        output.WriteRawTag(48);
        output.WriteBool(Testbool);
      }
      if (Testfloat != 0F) {
        output.WriteRawTag(61);
        output.WriteFloat(Testfloat);
      }
      if (Testint32 != 0) {
        output.WriteRawTag(64);
        output.WriteInt32(Testint32);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (Id != 0) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(Id);
      }
      if (Name.Length != 0) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Name);
      }
      size += bags_.CalculateSize(_map_bags_codec);
      if (Jsontest.Length != 0) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Jsontest);
      }
      size += skillLevel_.CalculateSize(_repeated_skillLevel_codec);
      if (Testbool != false) {
        size += 1 + 1;
      }
      if (Testfloat != 0F) {
        size += 1 + 4;
      }
      if (Testint32 != 0) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(Testint32);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(Test other) {
      if (other == null) {
        return;
      }
      if (other.Id != 0) {
        Id = other.Id;
      }
      if (other.Name.Length != 0) {
        Name = other.Name;
      }
      bags_.Add(other.bags_);
      if (other.Jsontest.Length != 0) {
        Jsontest = other.Jsontest;
      }
      skillLevel_.Add(other.skillLevel_);
      if (other.Testbool != false) {
        Testbool = other.Testbool;
      }
      if (other.Testfloat != 0F) {
        Testfloat = other.Testfloat;
      }
      if (other.Testint32 != 0) {
        Testint32 = other.Testint32;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 8: {
            Id = input.ReadInt32();
            break;
          }
          case 18: {
            Name = input.ReadString();
            break;
          }
          case 26: {
            bags_.AddEntriesFrom(input, _map_bags_codec);
            break;
          }
          case 34: {
            Jsontest = input.ReadString();
            break;
          }
          case 42:
          case 40: {
            skillLevel_.AddEntriesFrom(input, _repeated_skillLevel_codec);
            break;
          }
          case 48: {
            Testbool = input.ReadBool();
            break;
          }
          case 61: {
            Testfloat = input.ReadFloat();
            break;
          }
          case 64: {
            Testint32 = input.ReadInt32();
            break;
          }
        }
      }
    }

  }

  public sealed partial class TestConf : pb::IMessage<TestConf> {
    private static readonly pb::MessageParser<TestConf> _parser = new pb::MessageParser<TestConf>(() => new TestConf());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<TestConf> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::gen.rawdata.TestConfReflection.Descriptor.MessageTypes[1]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public TestConf() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public TestConf(TestConf other) : this() {
      testMap_ = other.testMap_.Clone();
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public TestConf Clone() {
      return new TestConf(this);
    }

    /// <summary>Field number for the "TestMap" field.</summary>
    public const int TestMapFieldNumber = 1;
    private static readonly pbc::MapField<string, global::gen.rawdata.Test>.Codec _map_testMap_codec
        = new pbc::MapField<string, global::gen.rawdata.Test>.Codec(pb::FieldCodec.ForString(10, ""), pb::FieldCodec.ForMessage(18, global::gen.rawdata.Test.Parser), 10);
    private readonly pbc::MapField<string, global::gen.rawdata.Test> testMap_ = new pbc::MapField<string, global::gen.rawdata.Test>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public pbc::MapField<string, global::gen.rawdata.Test> TestMap {
      get { return testMap_; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as TestConf);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(TestConf other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!TestMap.Equals(other.TestMap)) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      hash ^= TestMap.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      testMap_.WriteTo(output, _map_testMap_codec);
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      size += testMap_.CalculateSize(_map_testMap_codec);
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(TestConf other) {
      if (other == null) {
        return;
      }
      testMap_.Add(other.testMap_);
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 10: {
            testMap_.AddEntriesFrom(input, _map_testMap_codec);
            break;
          }
        }
      }
    }

  }

  #endregion

}

#endregion Designer generated code
