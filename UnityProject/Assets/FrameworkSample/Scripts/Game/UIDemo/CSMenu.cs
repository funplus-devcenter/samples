﻿using UnityEngine;
using UnityEngine.EventSystems;
using CenturyGame.Framework.UI;
using GameLogic;

public class CSMenu : MonoBehaviour
{
    private void Awake()
    {
        var btn_chat = transform.Find("Btn_chat").gameObject;
        EventTriggerListener.GetListener(btn_chat).onPointerClick += OnClickChat;
        var btn_config = transform.Find("Btn_config").gameObject;
        EventTriggerListener.GetListener(btn_config).onPointerClick += OnClickConfig;
    }

    private void OnClickChat(GameObject go, PointerEventData eventData)
    {
        GameEntry.UI.OpenWindow("chatform_cs", EWindowLayer.Pop);
    }

    private void OnClickConfig(GameObject go, PointerEventData eventData)
    {
        GameEntry.UI.OpenWindow("dataform_cs", EWindowLayer.Pop);
    }
}
