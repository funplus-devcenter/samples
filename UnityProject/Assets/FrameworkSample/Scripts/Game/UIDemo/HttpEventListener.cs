﻿using ProtokitHelper;
using GameLogic.UI;

namespace GameLogic
{
    public static class HttpEventListener
    {
        public static void AddEventListener()
        {
            GameEntry.HttpClient.Evt_RecvCommonError += GetProtoCommonError;
            GameEntry.HttpClient.Evt_RspFailed += GetHttpError;
            GameEntry.HttpClient.Evt_ReqBegin += HttpRequestBegin;
            GameEntry.HttpClient.Evt_ReqEnd += HttpRequestEnd;
            GameEntry.HttpClient.Evt_ReqFailed += HttpRequestFailed;
            GameEntry.HttpClient.Evt_ReqTimeOutExpect += HttpRequestTimeOutExpect;
        }

        public static void GetProtoCommonError(int code, string message)
        {
            Tips.ShowMessage($"Get proto common error, code={code}, message={message}", "Common Error");
        }

        public static void GetHttpError(int statusCode, string statusName)
        {
            Tips.ShowMessage($"Get http error status, code={statusCode}, name={statusName}", "HTTP Error");
        }

        public static void HttpRequestBegin()
        {
        }

        public static void HttpRequestEnd()
        {
        }

        public static void HttpRequestFailed()
        {
            Tips.ShowMessage("Http request Failed", "HTTP Request Failed");
            GameEntry.HttpClient.Init(false, false);
        }

        public static void HttpRequestTimeOutExpect()
        {

        }
    }
}