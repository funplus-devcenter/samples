﻿using UnityEngine;
using System;
using System.Collections.Generic;
using CenturyGame.AssetBundleManager.Runtime;

namespace GameLogic.UI
{
    public static class Tips
    {
        private static UnityEngine.Object prefab;
        private static Transform tipsRoot;
        private static readonly int GroupDepth = 7000;
        private static readonly int DepthFactor = 10;
        private static List<TipsDialog> tipsList = new List<TipsDialog>(16);

        private static TipsDialog GetTipDialog()
        {
            if (prefab == null)
            {
                string prefabPath = "ui/tipdialog";
                prefab = AssetBundleManager.Load(prefabPath, typeof(UnityEngine.Object));
            }
            if (tipsRoot == null)
            {
                tipsRoot = GameObject.Find("UIRoot/Top").transform;
            }
            GameObject go = UnityEngine.Object.Instantiate(prefab) as GameObject;
            go.transform.SetParent(tipsRoot);
            go.name = "tipdialog";
            TipsDialog dialog = go.GetComponent<TipsDialog>();
            return dialog;
        }

        public static void ShowConfirm(string content, string title = "Message", Action onConfirm = null, Action onCancel = null)
        {
            TipsDialog dialog = GetTipDialog();
            tipsList.Add(dialog);
            RefreshTipsDepth();
            dialog.InitConfirmDialog(title, content, onConfirm, onCancel);
        }

        public static void ShowMessage(string content, string title = "Message", Action onOk = null)
        {
            TipsDialog dialog = GetTipDialog();
            tipsList.Add(dialog);
            RefreshTipsDepth();
            dialog.InitMsgDialog(title, content, onOk);
        }

        public static void CloseTips(TipsDialog dialog)
        {
            tipsList.Remove(dialog);
            UnityEngine.Object.Destroy(dialog.gameObject);
            RefreshTipsDepth();
        }

        public static void RefreshTipsDepth()
        {
            for (int i = 0; i < tipsList.Count; i++)
            {
                tipsList[i].RefreshDepth(GroupDepth, i, DepthFactor);
            }
        }
    }
}