﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace GameLogic.UI
{
    public class TipsDialog : MonoBehaviour
    {
        private Button btn_ok;
        private Button btn_confirm;
        private Button btn_cancel;
        private Text txt_title;
        private Text txt_content;
        private Canvas canvas;

        private void Awake()
        {
            canvas = gameObject.GetComponent<Canvas>();
            txt_title = transform.Find("txt_title").GetComponent<Text>();
            txt_content = transform.Find("txt_content").GetComponent<Text>();
            btn_ok = transform.Find("btn_ok").GetComponent<Button>();
            btn_confirm = transform.Find("btn_confirm").GetComponent<Button>();
            btn_cancel = transform.Find("btn_cancel").GetComponent<Button>();
        }

        private Action cb_ok;
        private Action cb_confirm;
        private Action cb_cancel;

        public void InitMsgDialog(string title, string content, Action onOk)
        {
            txt_title.text = title;
            txt_content.text = content;
            cb_ok = onOk;
            cb_confirm = null;
            cb_cancel = null;
            if (btn_confirm.gameObject.activeSelf)
                btn_confirm.gameObject.SetActive(false);
            if (btn_cancel.gameObject.activeSelf)
                btn_cancel.gameObject.SetActive(false);
            if (!btn_ok.gameObject.activeSelf)
                btn_ok.gameObject.SetActive(true);
        }

        public void InitConfirmDialog(string title, string content, Action onConfirm, Action onCancel)
        {
            txt_title.text = title;
            txt_content.text = content;
            cb_ok = null;
            cb_confirm = onConfirm;
            cb_cancel = onCancel;
            if (!btn_confirm.gameObject.activeSelf)
                btn_confirm.gameObject.SetActive(true);
            if (!btn_cancel.gameObject.activeSelf)
                btn_cancel.gameObject.SetActive(true);
            if (btn_ok.gameObject.activeSelf)
                btn_ok.gameObject.SetActive(false);
        }

        public void OnClickOk()
        {
            cb_ok?.Invoke();
            Tips.CloseTips(this);
        }

        public void OnClickConfirm()
        {
            cb_confirm?.Invoke();
            Tips.CloseTips(this);
        }

        public void OnClickCancel()
        {
            cb_cancel?.Invoke();
            Tips.CloseTips(this);
        }

        public void RefreshDepth(int groupDepth, int depthInGroup, int depthFactor)
        {
            canvas.sortingOrder = groupDepth + depthInGroup * depthFactor;
        }
    }
}