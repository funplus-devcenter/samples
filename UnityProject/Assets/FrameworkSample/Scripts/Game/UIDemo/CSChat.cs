﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using gen.msg;
using gen.netutils;
using CenturyGame.Framework.UI;
using Google.Protobuf;

namespace GameLogic
{
    public class CSChat : MonoBehaviour
    {
        private Text txt_content;
        private InputField ip_chat;

        private void Awake()
        {
            txt_content = transform.Find("txt_content").GetComponent<Text>();
            ip_chat = transform.Find("input_chat").GetComponent<InputField>();
            var btn_tcp_chat = transform.Find("btn_tcp_chat").gameObject;
            var btn_post_chat = transform.Find("btn_http_post").gameObject;
            var btn_get_chat = transform.Find("btn_http_get").gameObject;
            var btn_back = transform.Find("btn_back").gameObject;
            var btn_clear = transform.Find("btn_clear").gameObject;
            var btn_disorder = transform.Find("btn_http_disorder").gameObject;
            EventTriggerListener.GetListener(btn_tcp_chat).onPointerClick += OnClickTCPChat;
            EventTriggerListener.GetListener(btn_post_chat).onPointerClick += OnClickHttpPostChat;
            EventTriggerListener.GetListener(btn_get_chat).onPointerClick += OnClickHttpGetChat;
            EventTriggerListener.GetListener(btn_back).onPointerClick += OnClickBtnBack;
            EventTriggerListener.GetListener(btn_clear).onPointerClick += OnClickBtnClear;
            EventTriggerListener.GetListener(btn_disorder).onPointerClick += OnClickHttpDisorder;
            GameEntry.TcpClient.RegisterMessageHandler(Chat.Descriptor.FullName, OnChat);
            GameEntry.HttpClient.RegisterMessageHandler(NormalAck.Descriptor.FullName, OnHttpNormalAck);
        }

        private void OnDestroy()
        {
            GameEntry.TcpClient.RemoveMessageHandler(Chat.Descriptor.FullName, OnChat);
            GameEntry.HttpClient.RemoveMessageHandler(NormalAck.Descriptor.FullName, OnHttpNormalAck);
        }

        private void OnClickTCPChat(GameObject go, PointerEventData eventData)
        {
            Chat msg = new Chat
            {
                Text = $"player {CSLogin.Id} say: {ip_chat.text}  (cs tcp)"
            };
            GameEntry.TcpClient.SendMessage(msg);
        }

        private void OnClickHttpPostChat(GameObject go, PointerEventData eventData)
        {
            Chat msg = new Chat
            {
                Text = $"player {CSLogin.Id} say: {ip_chat.text}  (cs http/post)",
            };
            GameEntry.HttpClient.PostRequestMsg("http://10.0.84.202:8991/api/v1", msg, delegate (IMessage rsp)
            {
                if (rsp.Descriptor.FullName.Equals(NormalAck.Descriptor.FullName))
                    Debug.Log("Http Post Chat Success");
                else
                    Debug.Log("Http Post Chat Failed");
            });
        }

        private void OnClickHttpGetChat(GameObject go, PointerEventData eventData)
        {
            var header = new Dictionary<string, string>
            {
                {"x-fun-user-token", CSLogin.Token}, {"charset", "utf-8"},
                {"x-fun-request-id", (DateTime.Now).ToString("yyyyMMddHHmmssffff")}
            };
            var data = new Dictionary<string, string>
            {
                {"PlayerId", CSLogin.Id.ToString() },
                {"text", $"player {CSLogin.Id} say: {ip_chat.text}  (cs http/get)"}
            };
            GameEntry.Http.GetHttpRequest("http://10.0.84.202:8991/api/v1/chat", header, data, delegate (byte[] recvData)
            {
                Debug.Log("http get back");
            });
        }

        private void OnClickHttpDisorder(GameObject go, PointerEventData eventData)
        {
            MockMessageReq msg = new MockMessageReq
            {
                Type = MockType.MockNormal,
                NetScheme = "HttpClient"
            };
            GameEntry.HttpClientDisorder.PostRequestMsg("http://zhongtai-demo-game-sandbox.10.0.4.100.xip.name/api/v0/Mock", msg, delegate (IMessage rsp)
            {
                if (rsp.Descriptor.FullName.Equals(MockMessageRsp.Descriptor.FullName))
                {
                    var mock = rsp as MockMessageRsp;
                    Debug.Log($"Receive Mock, {mock}");
                }
                else
                    Debug.Log("HttpDisorder Response Failed.");
            });
        }

        private void OnChat(IMessage msg)
        {
            var rsp = msg as Chat;
            txt_content.text = txt_content.text + rsp.Text + "\n";
        }

        private void OnHttpNormalAck(IMessage msg)
        {
            Debug.Log("OnHttpNormalAck");
        }

        private void OnClickBtnBack(GameObject go, PointerEventData eventData)
        {
            GameEntry.UI.CloseWindow("chatform_cs", false);
        }

        private void OnClickBtnClear(GameObject go, PointerEventData eventData)
        {
            txt_content.text = string.Empty;
        }
    }
}