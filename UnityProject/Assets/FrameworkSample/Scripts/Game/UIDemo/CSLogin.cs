﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using CenturyGame.Framework.UI;
using gen.msg;
using gen.netutils;
using Google.Protobuf;

namespace GameLogic
{
    public class CSLogin : MonoBehaviour
    {
        public static string Token { get; private set; }
        public static ulong Id { get; private set; }

        private InputField ip_user;

        private readonly string Host = "10.0.84.202";
        private readonly int Port = 8990;

        private void Awake()
        {
            ip_user = transform.Find("input_user").GetComponent<InputField>();
            var btn_login = transform.Find("btn_login").gameObject;
            EventTriggerListener.GetListener(btn_login).onPointerClick += OnClickLogin;
            var btn_http_login = transform.Find("btn_http_login").gameObject;
            EventTriggerListener.GetListener(btn_http_login).onPointerClick += OnClickHttpLogin;
        }

        private void OnEnable()
        {
            GameEntry.Network.onConnectSuccess += OnConnectSuccess;
            GameEntry.Network.onConnectFailed += OnConnectFailed;
            GameEntry.Network.onDisconnect += OnDisconnect;
        }

        private void OnDisable()
        {
            GameEntry.Network.onConnectSuccess -= OnConnectSuccess;
            GameEntry.Network.onConnectFailed -= OnConnectFailed;
            GameEntry.Network.onDisconnect -= OnDisconnect;
        }

        public void OnClickLogin(GameObject go, PointerEventData eventData)
        {
            GameEntry.Network.StartTcpConnect(Host, Port);
        }

        public void OnClickHttpLogin(GameObject go, PointerEventData eventData)
        {
            Authorize msg = new Authorize
            {
                PlayerID = ulong.Parse(ip_user.text)
            };
            GameEntry.HttpClient.PostRequestMsg("http://10.0.84.202:8991/auto", msg, OnAuthorize);
        }

        private void OnConnectSuccess()
        {
            Debug.Log("connect success");
            Authorize msg = new Authorize
            {
                PlayerID = ulong.Parse(ip_user.text)
            };
            GameEntry.TcpClient.SendMessage(msg, OnAuthorize);
        }

        private void OnConnectFailed()
        {
            Debug.Log("connect failed");
        }

        private void OnDisconnect(string reason)
        {
            Debug.Log("disconnect. reason:" + reason);
        }

        private void OnAuthorize(IMessage msg)
        {
            if (msg.Descriptor.FullName.Equals(Authorize.Descriptor.FullName))
            {
                var rsp = msg as Authorize;
                Id = rsp.PlayerID;
                Token = rsp.GameAccessToken;
                Debug.Log("OnAuthorize. IsSuccess:" + rsp.IsSuccess + ", playerId:" + rsp.PlayerID + ", token:" + rsp.GameAccessToken);
                GameEntry.UI.CloseWindow("loginform_cs", true);
                GameEntry.UI.OpenWindow("mainform_cs", EWindowLayer.Common);
            }
            else if (msg.Descriptor.FullName.Equals(ErrorResponse.Descriptor.FullName))
            {
                var errRsp = msg as ErrorResponse;
                Debug.LogWarning($"request Authorize response Error, code={errRsp.Code}, message={errRsp.Message}");
            }
            else
                Debug.LogWarning($"request Authorize response unexcepect proto, uri={msg.Descriptor.FullName}");
        }
    }
}