﻿using UnityEngine;
using CenturyGame.Framework;
using CenturyGame.Framework.UI;
using CenturyGame.Framework.Network;
using CenturyGame.Framework.Http;
using CenturyGame.Framework.Lua;
using ProtokitHelper;
using CenturyGame.LuaModule.Runtime;
using CenturyGame.LuaModule.Runtime.Interfaces;

namespace GameLogic
{
    public class GameEntry : IGameEntry
    {
        public static UIManager UI
        {
            get;
            private set;
        }

        public static NetworkManager Network
        {
            get;
            private set;
        }

        public static LuaManager Lua
        {
            get;
            private set;
        }

        public static HttpManager Http
        {
            get;
            private set;
        }

        public static ProtokitClient TcpClient
        {
            get { return ProtokitClient.Instance; }
        }

        public static ProtokitHttpClient HttpClient
        {
            get { return ProtokitHttpClient.Instance; }
        }

        public static ProtoKitHttpClientDisorder HttpClientDisorder
        {
            get { return ProtoKitHttpClientDisorder.Instance; }
        }

        public void StartGame()
        {
            //初始化各模块
            InitFrameworkModules();
            //初始化网络消息处理器
            Network.SetMsgProcesser(ProtokitMsgProcessor.Instance);
            //初始化消息解析器字典
            ProtokitUtil.Instance.Init();
            //开始LuaDemo
            StartLuaDemo();
            //开始C#Demo
            //StartCSDemo();
        }

        private void InitFrameworkModules()
        {
            Lua = GameLauncher.Lua;
            UI = GameLauncher.UI;
            Network = GameLauncher.Network;
            Http = GameLauncher.Http;
        }

        private void StartLuaDemo()
        {
            var luaPlugin = LuaPluginCreateFactory.Create("CenturyGame.LuaModule.XLuaSpecialized.XLuaPlugin");
            Lua.SetLuaPlugin(luaPlugin);
            LuaAccessManager.Environment.DoString("require 'main'");

            //初始化TCP接口
            ILuaTable TCPClient_lua = LuaAccessManager.GetGlobalTable("ProtokitClient");
            ILuaFunction TCPInitFunction = TCPClient_lua.GetFunction("Init");
            TCPInitFunction?.Call();

            //初始化HTTP接口
            ILuaTable HTTPClient_lua = LuaAccessManager.GetGlobalTable("ProtokitHttp");
            ILuaFunction HTTPInitFunction = HTTPClient_lua.GetFunction("Init");
            HTTPInitFunction?.Call();

            //初始哈HTTP服务端保序接口
            ILuaTable HTTPDisorder_lua = LuaAccessManager.GetGlobalTable("ProtokitHttpDisorder");
            ILuaFunction DisorderInitFunction = HTTPDisorder_lua.GetFunction("Init");
            DisorderInitFunction?.Call();

            //打开Lua逻辑的登录界面
            UI.OpenWindow("loginform", EWindowLayer.Common);
        }

        private void StartCSDemo()
        {
            //初始化TCP接口
            TcpClient.Init(false);

            //初始化HTTP接口
            HttpClient.Init(false, true);

            //初始化HttpDisorder接口
            HttpClientDisorder.Init(true, 3);

            //添加HTTP事件监听
            HttpEventListener.AddEventListener();

            //打开CS逻辑的登录界面
            UI.OpenWindow("loginform_cs", EWindowLayer.Common);
        }

        public void Update()
        {
            TcpClient.Update();
            HttpClient.Update();
            HttpClientDisorder.Update();
        }

        public void LateUpdate()
        {
        }

        public void FixedUpdate()
        {
        }

        public void OnApplicationFocus(bool focus)
        {
        }

        public void OnApplicationPause(bool pause)
        {
        }

        public void QuitGame()
        {
        }
    }
}