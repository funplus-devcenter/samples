﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using CenturyGame.Framework.UI;
using CenturyGame.AppUpdaterLib.Runtime;

namespace GameLogic
{
    public class CSDataForm : MonoBehaviour
    {
        private InputField input_id;
        private Text txt_name;

        private void Awake()
        {
            txt_name = transform.Find("txt_name").GetComponent<Text>();
            input_id = transform.Find("ip_id").GetComponent<InputField>();
            var btn_query = transform.Find("btn_query").gameObject;
            var btn_back = transform.Find("btn_back").gameObject;
            EventTriggerListener.GetListener(btn_query).onPointerClick += OnClickQuery;
            EventTriggerListener.GetListener(btn_back).onPointerClick += OnClickBack;
            loadConfigs();
        }

        private void loadConfigs()
        {
            string configFolderPath = Application.streamingAssetsPath + "/data/";
            //var e = conf.Gen.Loaders.GetEnumerator();
            //while (e.MoveNext())
            //{
            //    string path = configFolderPath + e.Current.Key;
            //    var stream = new Google.Protobuf.CodedInputStream(System.IO.File.ReadAllBytes(path));
            //    e.Current.Value(stream);
            //    Debug.Log($"Load {e.Current.Key}");
            //}
            //string path = configFolderPath + "AbilityBuffSpecConf.bytes";
            //var stream = new Google.Protobuf.CodedInputStrea(System.IO.File.ReadAllBytes(path));
            //gen.conf.Data.LoadAbilityBuffSpecFromStream(stream);

            string configDownloadPath = AssetsFileSystem.GetWritePath($"data/AbilityBuffSpecConf.bytes");
            if (System.IO.File.Exists(configDownloadPath))
            {
                Debug.Log("Load From Download");
                var dataStream = new Google.Protobuf.CodedInputStream(System.IO.File.ReadAllBytes(configDownloadPath));
                gen.conf.Data.LoadAbilityBuffSpecFromStream(dataStream);
            }
            else
            {
                Debug.Log("Load From StreamingAssets");
                string path = configFolderPath + "AbilityBuffSpecConf.bytes";
                var stream = new Google.Protobuf.CodedInputStream(System.IO.File.ReadAllBytes(path));
                gen.conf.Data.LoadAbilityBuffSpecFromStream(stream);
            }
        }

        private void OnClickBack(GameObject go, PointerEventData eventData)
        {
            GameEntry.UI.CloseWindow("dataform_cs", true);
        }

        private void OnClickQuery(GameObject go, PointerEventData eventData)
        {
            int id = int.Parse(input_id.text);
            //var config = conf.Gen.GetAbilityBuffSpec(id);
            var config = gen.conf.Data.GetAbilityBuffSpec(id);
            if (config != null)
                txt_name.text = $"key={config.SpecId}, ControllerClassName={config.ControllerClassName}";
            else
                txt_name.text = "查询的配置id不存在";
        }
    }
}