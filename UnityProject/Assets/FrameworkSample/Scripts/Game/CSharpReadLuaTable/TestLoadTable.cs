﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua.LuaModule.XLuaSpecialized;

public class TestLoadTable : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnGUI()
    {
        if (GUILayout.Button("Get"))
        {
            var table = XLuaDataAccessor.GetData("ResourceVersion", "6.0.000");

            if (table != null)
            {
                Debug.LogError($"AppVersion : {table.Get<string>("AppVersion")}");
                Debug.LogError($"ResAndoridVersion : {table.Get<string>("ResAndoridVersion")}");
                Debug.LogError($"ResIOSVersion : {table.Get<string>("ResIOSVersion")}");
            }
            Debug.LogError($"table is null : {table == null}");
        }
    }
}
