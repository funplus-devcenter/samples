﻿using System.Collections;
using System.Collections.Generic;
using CenturyGame.Framework;
using CenturyGame.Framework.Lua;
using CenturyGame.LuaModule.Runtime;
using UnityEngine;

public class CSharpReadLuaTableEntry : IGameEntry
{
    public static LuaManager Lua
    {
        get;
        private set;
    }

    public void StartGame()
    {
        Lua = GameLauncher.Lua;
        var luaPlugin = LuaPluginCreateFactory.Create("CenturyGame.LuaModule.XLuaSpecialized.XLuaPlugin");
        Lua.SetLuaPlugin(luaPlugin);
        LuaAccessManager.Environment.DoString("require 'main'");
    }

    public void Update()
    {
    }

    public void FixedUpdate()
    {
    }

    public void LateUpdate()
    {
    }

    public void OnApplicationFocus(bool focus)
    {
    }

    public void OnApplicationPause(bool pause)
    {
    }

    public void QuitGame()
    {
    }
}
