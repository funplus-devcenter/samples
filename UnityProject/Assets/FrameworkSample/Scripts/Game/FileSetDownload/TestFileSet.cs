﻿using System;
using System.Collections;
using System.Collections.Generic;
using CenturyGame.FilesDeferredDownloader.Runtime;
using UnityEngine;

public class TestFileSet : MonoBehaviour
{
    private ProgressData mProgressData;
    // Start is called before the first frame update
    void Start()
    {
        FilesDeferredDownloadManager.Init(result1 =>
        {
            if (result1)
            {
                this.mProgressData = FilesDeferredDownloadManager.GetProgressData();

                FilesDeferredDownloadManager.SetDownloadCompletedCallBack(() => { Debug.Log("Downloaded completed!"); });
                FilesDeferredDownloadManager.SetDownloadErrorCallBack(erorType => { Debug.LogError($"Error type : {erorType}"); });
                FilesDeferredDownloadManager.SetOnFileDownloadCallBack((result, name, size, hash) =>
                {
                    Debug.LogError($"frameidx : {Time.frameCount}  [result : {result}] [name : {name}] [size : {size}] [hash : {hash}]");
                });
                FilesDeferredDownloadManager.SetOnFileSetDownloadCallBack((result, fileSetName, md5) =>
                {
                    Debug.LogError($"Download file set reset : {result} , fileSetName : {fileSetName} , md5 : {md5}");
                });
                Debug.Log("Set callback .");
            }
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string[] fileSetNames = { "F0.json", "F1.json" };

    private void OnGUI()
    {
        GUILayout.BeginHorizontal();

        GUILayout.BeginVertical();
        if (GUILayout.Button("Load file set"))
        {
            bool addedResult;
            FilesDeferredDownloadManager.SyncFiles(out addedResult,fileSetNames);
            Debug.LogError($"addedResult : {addedResult}");
        }

        GUILayout.EndVertical();
        GUILayout.BeginVertical();
        if (this.mProgressData != null)
        {
            GUILayout.Label($"FileSetName : {this.mProgressData.FileSetName}");
            GUILayout.Label($"Progress : {this.mProgressData.Progress:f3}");
            GUILayout.Label($"CurrentDownloadingFileProgress : {this.mProgressData.CurrentDownloadingFileProgress:f3}");
            GUILayout.Label($"CurrentDownloadedFileCount : {this.mProgressData.CurrentDownloadedFileCount}");
            GUILayout.Label($"TotalDownloadFileCount : {this.mProgressData.TotalDownloadFileCount}");
            GUILayout.Label($"CurrentDownloadingFileSize : {this.mProgressData.CurrentDownloadingFileSize}");
            GUILayout.Label($"CurrentDownloadedFileSize : {this.mProgressData.CurrentDownloadedFileSize}");
            GUILayout.Label($"TotalDownloadSize: {this.mProgressData.TotalDownloadSize}");

        }

        //if (GUILayout.Button("Download"))
        //{
        //    this.TestException();
        //}
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
    }


    //private void TestException()
    //{
    //    try
    //    {
    //        Call1();
    //    }
    //    finally
    //    {
    //        Debug.LogError("Finally");
    //    }
    //}

    //private void Call1()
    //{
    //    Debug.LogError("Call1");
    //    throw new InvalidOperationException("Call1");
    //}
}
