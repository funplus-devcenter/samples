﻿using System.Collections;
using System.Collections.Generic;
using CenturyGame.AppUpdaterLib.Runtime.Managers;
using CenturyGame.FilesDeferredDownloader.Runtime;
using CenturyGame.Framework;
using CenturyGame.Framework.Http;
using CenturyGame.Game;
using UnityEngine;

namespace GameLogic
{
    public class FileSetDownloadEntry : IGameEntry
    {
        public void StartGame()
        {
            
        }

        public void Update()
        {
        }

        public void LateUpdate()
        {
        }

        public void FixedUpdate()
        {
        }

        public void QuitGame()
        {
        }

        public void OnApplicationFocus(bool focus)
        {
        }

        public void OnApplicationPause(bool pause)
        {
        }
    }
}


