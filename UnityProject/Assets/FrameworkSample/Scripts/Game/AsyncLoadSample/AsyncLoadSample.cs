﻿using UnityEngine;
using UnityEngine.Video;
using CenturyGame.AssetBundleManager.Runtime;

public class AsyncLoadSample : MonoBehaviour
{
    private GameObject videoObj;
    private VideoPlayer vPlayer;
    private GameObject uiObj;
    private AudioSource aPlayer;

    private void Start()
    {
        videoObj = new GameObject("video");
        vPlayer = videoObj.AddComponent<VideoPlayer>();
        vPlayer.playOnAwake = false;
        vPlayer.renderMode = VideoRenderMode.CameraNearPlane;
        vPlayer.targetCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        vPlayer.isLooping = true;
        GameObject audioObj = new GameObject("audio");
        aPlayer = audioObj.AddComponent<AudioSource>();
        aPlayer.playOnAwake = false;
        aPlayer.loop = true;
    }

    private void OnGUI()
    {
        if (GUILayout.Button("异步Load视频", GUILayout.Width(200), GUILayout.Height(80)))
        {
            AssetBundleManager.LoadAsync("movies/1", typeof(VideoClip), delegate (Object obj)
            {
                VideoClip video = obj as VideoClip;
                vPlayer.clip = video;
                vPlayer.Play();
            });
        }
        if (GUILayout.Button("UnLoad视频", GUILayout.Width(200), GUILayout.Height(80)))
        {
            vPlayer.Stop();
            vPlayer.clip = null;
            AssetBundleManager.Release("movies/1");
        }
        if (GUILayout.Button("异步创建界面", GUILayout.Width(200), GUILayout.Height(80)))
        {
            AssetBundleManager.LoadAndInitAsync("ui/dataform_cs", delegate (GameObject gameobject)
            {
                uiObj = gameobject;
            });
        }
        if (GUILayout.Button("UnLoad界面", GUILayout.Width(200), GUILayout.Height(80)))
        {
            AssetBundleManager.Release(uiObj);
        }
        if (GUILayout.Button("异步加载音频", GUILayout.Width(200), GUILayout.Height(80)))
        {
            AssetBundleManager.LoadAsync("audio/mus_battle_02", typeof(AudioClip), delegate (Object obj)
            {
                AudioClip audio = obj as AudioClip;
                aPlayer.clip = audio;
                aPlayer.Play();
            });
        }
        if (GUILayout.Button("UnLoad音频", GUILayout.Width(200), GUILayout.Height(80)))
        {
            aPlayer.Stop();
            aPlayer.clip = null;
            AssetBundleManager.Release("audio/mus_battle_02");
        }
    }
}