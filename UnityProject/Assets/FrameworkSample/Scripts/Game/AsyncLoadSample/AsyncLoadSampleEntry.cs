﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CenturyGame.Framework;

public class AsyncLoadSampleEntry : IGameEntry
{
    private GameObject core;

    public void StartGame()
    {
        core = new GameObject("core");
        core.AddComponent<AsyncLoadSample>();
        Object.DontDestroyOnLoad(core);
    }

    public void Update()
    {
    }

    public void FixedUpdate()
    {
    }

    public void LateUpdate()
    {
    }

    public void OnApplicationFocus(bool focus)
    {
    }

    public void OnApplicationPause(bool pause)
    {
    }

    public void QuitGame()
    {
    }
}
