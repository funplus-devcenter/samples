﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace MockTest
{
    public class WaitDialog : MonoBehaviour
    {
        private Text txt_wait;
        private float time;

        private void Awake()
        {
            txt_wait = transform.Find("txt_wait").GetComponent<Text>();
            txt_wait.text = ".";
        }

        private void Update()
        {
            time += Time.deltaTime;
            if (time > 6.0f)
            {
                time = 0;
                txt_wait.text = ".";
            }
            else if (time > 5.0f)
            {
                txt_wait.text = "......";
            }
            else if (time > 4.0f)
            {
                txt_wait.text = ".....";
            }
            else if (time > 3.0f)
            {
                txt_wait.text = "....";
            }
            else if (time > 2.0f)
            {
                txt_wait.text = "...";
            }
            else if (time > 1.0f)
            {
                txt_wait.text = "..";
            }
        }
    }
}