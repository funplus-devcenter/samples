﻿using CenturyGame.Framework;
using CenturyGame.Framework.UI;
using CenturyGame.Framework.Network;
using ProtokitHelper;

namespace MockTest
{
    public class MockTestEntry : IGameEntry
    {
        public static UIManager UI
        {
            get;
            private set;
        }

        public static NetworkManager Network
        {
            get;
            private set;
        }

        public static ProtokitHttpClient HttpClient
        {
            get { return ProtokitHttpClient.Instance; }
        }

        public static ProtoKitHttpClientDisorder HttpClientDisorder
        {
            get { return ProtoKitHttpClientDisorder.Instance; }
        }

        public void StartGame()
        {
            UI = GameLauncher.UI;
            Network = GameLauncher.Network;
            Network.SetMsgProcesser(ProtokitMsgProcessor.Instance);
            ProtokitUtil.Instance.Init();
            HttpClient.Init(true, true, 10);
            HttpClient.Evt_ReqFailed += HttpRequestFailed;
            HttpClient.Evt_ReqTimeOutExpect += HttpRequestOutExpectTime;
            HttpClient.Evt_ReqEnd += HttpRequestEnd;
            HttpClient.Evt_RecvCommonError += HttpRequestCommonError;
            HttpClientDisorder.Init(false);
            HttpClientDisorder.Evt_ReqTimeOutExpect += HttpRequestOutExpectTime;
            HttpClientDisorder.Evt_RecvCommonError += HttpRequestCommonError;
            HttpClientDisorder.Evt_ReqEnd += HttpRequestEnd;
            UI.OpenWindow("mockform_cs", EWindowLayer.Common);
        }

        public void Update()
        {
            HttpClient.Update();
            HttpClientDisorder.Update();
        }

        public void LateUpdate()
        {
        }

        public void FixedUpdate()
        {
        }

        public void OnApplicationFocus(bool focus)
        {
        }

        public void OnApplicationPause(bool pause)
        {
        }

        public void QuitGame()
        {
        }

        private void HttpRequestFailed()
        {
            GameLogic.UI.Tips.ShowConfirm("Http请求失败，是否重试？", "提示", delegate ()
            {
                HttpClient.Retry();
            }, delegate ()
            {
                HttpClient.Stop();
                HttpClient.Init(true, true, 10);
            });
        }

        private void HttpRequestOutExpectTime()
        {
            WaitDialogMgr.ShowWaitDialog();
        }

        private void HttpRequestEnd()
        {
            WaitDialogMgr.CloseWaitDialog();
        }

        private void HttpRequestCommonError(int code, string message)
        {
            GameLogic.UI.Tips.ShowMessage($"Http请求返回底层错误, code={code}, message={message}", "提示");
        }
    }
}
