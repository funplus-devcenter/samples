﻿using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using gen.msg;
using gen.netutils;
using Google.Protobuf;
using CenturyGame.AssetBundleManager.Runtime;
using ProtokitHelper;

namespace MockTest
{
    public class MockForm : MonoBehaviour
    {
        private Dropdown Dropdown_Scheme;
        private Dropdown Dropdown_KeepOrder;
        private InputField Input_delay;
        private Text txt_response;
        private InputField Input_connect_num;
        private InputField Input_connect_limit;
        private InputField Input_send_count;
        private InputField Input_simulate_num;
        private InputField Input_simulate_time;
        private Button btn_connect_test;
        private Text txt_rsp_count;
        private Text txt_process_count;
        private Text txt_cost_time;
        private StringBuilder sb = new StringBuilder();
        private readonly string URL = "http://zhongtai-demo-game-sandbox.10.0.4.100.xip.name/api/v0/Mock";
        private readonly string URL_ServerKeepOrder = "http://zhongtai-demo-game-sandbox.10.0.4.100.xip.name/api/v0/Mock";
        private readonly string URL_Cookies = "http://zhongtai-demo-game-sandbox.10.0.4.100.xip.name/api/v0/Mock";
        private string HttpScheme = "HttpClient";
        private bool serverKeepOrder = false;
        private int RequestCount = 0;
        private int ResponseCount = 0;
        private float startTime = 0;
        private HttpClient SimpleHttpClient = new HttpClient();
        private string server_key = string.Empty;
        private string affinity_key = string.Empty;
        private string session_key = string.Empty;
        private string cookies = string.Empty;

        private void Awake()
        {
            Application.targetFrameRate = -1;
            Dropdown_Scheme = transform.Find("Dropdown").GetComponent<Dropdown>();
            Dropdown_KeepOrder = transform.Find("dp_keep_order").GetComponent<Dropdown>();
            Input_delay = transform.Find("input_delay").GetComponent<InputField>();
            txt_response = transform.Find("ScrollView/Viewport/Content/txt_response").GetComponent<Text>();
            Input_connect_num = transform.Find("input_connect_num").GetComponent<InputField>();
            btn_connect_test = transform.Find("btn_connect_test").GetComponent<Button>();
            Input_connect_limit = transform.Find("input_connect_limit").GetComponent<InputField>();
            txt_rsp_count = transform.Find("txt_rsp_count").GetComponent<Text>();
            txt_process_count = transform.Find("txt_process_count").GetComponent<Text>();
            txt_process_count.text = SystemInfo.processorCount.ToString();
            Input_send_count = transform.Find("input_send_num").GetComponent<InputField>();
            txt_cost_time = transform.Find("txt_cost_time").GetComponent<Text>();
            Input_simulate_num = transform.Find("input_simulate_num").GetComponent<InputField>();
            Input_simulate_time = transform.Find("input_simulate_time").GetComponent<InputField>();
            MockTestEntry.HttpClient.Evt_RecvMsg += OnResponse;
        }

        public void OnClickNormal()
        {
            MockMessageReq req = new MockMessageReq
            {
                Type = MockType.MockNormal,
                NetScheme = HttpScheme
            };
            MockTestEntry.HttpClient.PostRequestMsg(URL, req);
        }

        public void OnClickDelay()
        {
            MockMessageReq req = new MockMessageReq
            {
                Type = MockType.MockDelay,
                NetScheme = HttpScheme,
                MinDelay = long.Parse(Input_delay.text),
                MaxDelay = long.Parse(Input_delay.text),
            };
            MockTestEntry.HttpClient.PostRequestMsg(URL, req);
        }

        public void OnClickDrop()
        {
            MockMessageReq req = new MockMessageReq
            {
                Type = MockType.MockDrop,
                NetScheme = HttpScheme
            };
            MockTestEntry.HttpClient.PostRequestMsg(URL, req);
        }

        public void OnClickGarbage()
        {
            AssetBundleManager.LoadAsync("rules/ai", typeof(TextAsset), delegate (UnityEngine.Object obj)
            {
                TextAsset textAsset = obj as TextAsset;
                MockMessageReq req = new MockMessageReq
                {
                    Type = MockType.MockGarbage,
                    NetScheme = HttpScheme,
                    GarbageSize = textAsset.bytes.LongLength,
                    UpData = ByteString.CopyFrom(textAsset.bytes)
                };
                MockTestEntry.HttpClient.PostRequestMsg(URL, req);
            });
        }

        public void OnClickError()
        {
            MockMessageReq req = new MockMessageReq
            {
                Type = MockType.MockError,
                NetScheme = HttpScheme
            };
            MockTestEntry.HttpClient.PostRequestMsg(URL, req);
        }

        public void OnClickConnectTest()
        {
            RequestCount = int.Parse(Input_connect_num.text);
            ResponseCount = 0;
            txt_rsp_count.text = string.Empty;
            MockMessageReq req = new MockMessageReq
            {
                Type = MockType.MockDelay,
                NetScheme = HttpScheme,
                MinDelay = long.Parse(Input_delay.text),
                MaxDelay = long.Parse(Input_delay.text),
            };
            for (int i = 0; i < RequestCount; i++)
            {
                if (HttpScheme.Equals("UnityWebRequest"))
                    UnityPost(req);
                else if (HttpScheme.Equals("HttpClient"))
                    SimplePostAsync(req);
            }
        }

        public void OnClickClearLog()
        {
            txt_response.text = string.Empty;
            txt_cost_time.text = string.Empty;
            txt_rsp_count.text = string.Empty;
        }

        public void OnClickSetLimit()
        {
            System.Net.ServicePointManager.DefaultConnectionLimit = int.Parse(Input_connect_limit.text);
        }

        public void OnHttpSchemeChange(int v)
        {
            if (Dropdown_Scheme.value == 0)
            {
                HttpScheme = "HttpClient";
                MockTestEntry.HttpClient.SwitchSchemeHttpClient();
            }
            else if (Dropdown_Scheme.value == 1)
            {
                HttpScheme = "UnityWebRequest";
                MockTestEntry.HttpClient.SwitchSchemeUnityWebRequest(this);
            }
        }

        private void OnResponse(int sequenceId, string passthrough, string uri, byte[] data)
        {
            ResponseCount++;
            txt_rsp_count.text = ResponseCount.ToString();
            var t = Time.realtimeSinceStartup - startTime;
            txt_cost_time.text = t.ToString("N2");
            if (uri.Equals(MockMessageRsp.Descriptor.FullName))
            {
                var rsp = MockMessageRsp.Parser.ParseFrom(data);
                sb.Length = 0;
                sb.Append(txt_response.text);
                sb.AppendLine($"Response MockMessageRsp, {rsp}, passthrough={passthrough}, sequenceId={sequenceId}");
                txt_response.text = sb.ToString();
            }
            else if (uri.Equals(ErrorResponse.Descriptor.FullName))
            {
                var err = ErrorResponse.Parser.ParseFrom(data);
                sb.Length = 0;
                sb.Append(txt_response.text);
                sb.AppendLine($"Response Error, {err}, passthrough={passthrough}, sequenceId={sequenceId}");
                txt_response.text = sb.ToString();
            }
            else
            {
                sb.Length = 0;
                sb.Append(txt_response.text);
                sb.AppendLine($"Recv Unexpect response, passthrough={passthrough}, sequenceId={sequenceId}, uri={uri}");
            }
        }

        private static byte[] ConverToBytes(IMessage msg, int sequenceId)
        {
            var rawProto = ProtokitUtil.Instance.GetRawPorto(msg);
            RawPacket rp = new RawPacket
            {
                Version = 1,
                SequenceID = sequenceId
            };
            RawAny rawAny = new RawAny
            {
                Uri = rawProto.MsgUri,
                Raw = rawProto.MsgRaw,
                PassThrough = rawProto.Passthrough
            };
            rp.RawAny.Add(rawAny);
            using (var stream = new MemoryStream())
            {
                rp.WriteTo(stream);
                return stream.ToArray();
            }
        }

        private void UnityPost(IMessage msg)
        {
            StartCoroutine(UnityPostCoroutine(msg));
        }

        private IEnumerator UnityPostCoroutine(IMessage msg)
        {
            int sequenceId = ProtokitUtil.Instance.GetSequenceId();
            byte[] sendData = ConverToBytes(msg, sequenceId);
            UnityWebRequest webReq = new UnityWebRequest(URL, "POST")
            {
                uploadHandler = new UploadHandlerRaw(sendData)
            };
            webReq.timeout = 200;
            webReq.SetRequestHeader("Content-Type", "application/x-protobuf");
            webReq.SetRequestHeader("x-fun-request-id", sequenceId.ToString());
            webReq.downloadHandler = new DownloadHandlerBuffer();
            yield return webReq.SendWebRequest();
            if (webReq.isNetworkError || webReq.isHttpError)
            {
                Debug.LogWarning($"PostAtOnce Request failed. statusCode={webReq.responseCode}. error={webReq.error}");
            }
            else
            {
                byte[] retData = webReq.downloadHandler.data;
                ParseResponse(retData);
            }
        }

        private void UnityPostServerPipeline(IMessage msg, Action action = null)
        {
            StartCoroutine(UnityPostServerPipelineCoroutine(msg, action));
        }

        private IEnumerator UnityPostServerPipelineCoroutine(IMessage msg, Action action = null)
        {
            int sequenceId = ProtokitUtil.Instance.GetSequenceId();
            byte[] sendData = ConverToBytes(msg, sequenceId);
            UnityWebRequest webReq = new UnityWebRequest(URL_ServerKeepOrder, "POST")
            {
                uploadHandler = new UploadHandlerRaw(sendData)
            };
            webReq.timeout = 20;
            webReq.SetRequestHeader("Content-Type", "application/x-protobuf");
            webReq.SetRequestHeader("x-fun-request-id", sequenceId.ToString());
            webReq.SetRequestHeader("x-pipeline-sequence-id", sequenceId.ToString());
            if (!string.IsNullOrEmpty(server_key))
                webReq.SetRequestHeader("x-pipeline-server-key", server_key);
            if (!string.IsNullOrEmpty(affinity_key))
                webReq.SetRequestHeader("x-pipeline-affinity-key", affinity_key);
            webReq.downloadHandler = new DownloadHandlerBuffer();
            yield return webReq.SendWebRequest();
            if (webReq.isNetworkError || webReq.isHttpError)
            {
                Debug.LogWarning($"PostAtOnce Request failed. statusCode={webReq.responseCode}. error={webReq.error}");
            }
            else
            {
                byte[] retData = webReq.downloadHandler.data;
                ParseResponse(retData);
                if (string.IsNullOrEmpty(server_key))
                    server_key = webReq.GetResponseHeader("x-pipeline-server-key");
                if (string.IsNullOrEmpty(affinity_key))
                    affinity_key = webReq.GetResponseHeader("x-pipeline-affinity-key");
                action?.Invoke();
            }
        }

        private void ParseResponse(byte[] data)
        {
            ResponseCount++;
            txt_rsp_count.text = ResponseCount.ToString();
            var t = Time.realtimeSinceStartup - startTime;
            txt_cost_time.text = t.ToString("N2");
            RawPacket rp;
            rp = RawPacket.Parser.ParseFrom(data);
            if (rp != null)
            {
                for (int i = 0; i < rp.RawAny.Count; i++)
                {
                    string name = rp.RawAny[i].Uri;
                    byte[] rawdata = rp.RawAny[i].Raw.ToByteArray();
                    string passthrough = rp.RawAny[i].PassThrough;
                    if (name.Equals(MockMessageRsp.Descriptor.FullName))
                    {
                        var rsp = MockMessageRsp.Parser.ParseFrom(rawdata);
                        sb.Length = 0;
                        sb.Append(txt_response.text);
                        sb.AppendLine($"Response MockMessageRsp, {rsp}, passthrough={passthrough}, sequenceId={rp.SequenceID}");
                        txt_response.text = sb.ToString();
                    }
                    else if (name.Equals(ErrorResponse.Descriptor.FullName))
                    {
                        var err = ErrorResponse.Parser.ParseFrom(data);
                        sb.Length = 0;
                        sb.Append(txt_response.text);
                        sb.AppendLine($"Response Error, {err}, passthrough={passthrough}, sequenceId={rp.SequenceID}");
                        txt_response.text = sb.ToString();
                    }
                }
            }
            else
                Debug.LogWarning("Parse RawPacket failed.");
        }

        private async void SimplePostAsync(IMessage msg)
        {
            try
            {
                int sequenceId = ProtokitUtil.Instance.GetSequenceId();
                byte[] sendData = ConverToBytes(msg, sequenceId);
                HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, URL)
                {
                    Content = new ByteArrayContent(sendData)
                };
                req.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-protobuf");
                req.Content.Headers.TryAddWithoutValidation("Connection", "keep-alive");
                req.Content.Headers.TryAddWithoutValidation("x-fun-request-id", sequenceId.ToString());
                req.Content.Headers.TryAddWithoutValidation("Content-Length", sendData.Length.ToString());
                CancellationTokenSource cancellationTokenSource = new CancellationTokenSource(200000);
                HttpResponseMessage rsp = await SimpleHttpClient.SendAsync(req, HttpCompletionOption.ResponseHeadersRead, cancellationTokenSource.Token);
                if (rsp.IsSuccessStatusCode)
                {
                    byte[] retData = await rsp.Content.ReadAsByteArrayAsync();
                    ParseResponse(retData);
                }
                else
                {
                    Debug.LogWarning($"SimplePostAsync Response Failed, StatusCode={rsp.StatusCode.ToString()}");
                }
            }
            catch (Exception e)
            {
                string exceptionType = "UnknownError";
                int statusCode = 0;
                if (e is ArgumentNullException)
                {
                    exceptionType = "ArgumentNull";
                }
                else if (e is InvalidOperationException)
                {
                    exceptionType = "InvalidOperation";
                }
                else if (e is HttpRequestException)
                {
                    if (e.InnerException is WebException)
                    {
                        WebException webException = e.InnerException as WebException;
                        exceptionType = webException.Status.ToString();
                        statusCode = (int)webException.Status;
                    }
                    else
                    {
                        HttpRequestException httpRequestException = e as HttpRequestException;
                        exceptionType = "HttpRequestException";
                        statusCode = httpRequestException.HResult;
                    }
                }
                else if (e is TaskCanceledException)
                {
                    exceptionType = "RequestTimeout";
                }
                Debug.LogWarning($"SimplePostAsync throw exception:{exceptionType}, code={statusCode.ToString()}");
            }
        }

        private async void SimplePostServerPipelineAsync(IMessage msg, Action action = null)
        {
            try
            {
                int sequenceId = ProtokitUtil.Instance.GetSequenceId();
                byte[] sendData = ConverToBytes(msg, sequenceId);
                HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, URL_ServerKeepOrder)
                {
                    Content = new ByteArrayContent(sendData)
                };
                req.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-protobuf");
                req.Content.Headers.TryAddWithoutValidation("Connection", "keep-alive");
                req.Content.Headers.TryAddWithoutValidation("x-fun-request-id", sequenceId.ToString());
                req.Content.Headers.TryAddWithoutValidation("Content-Length", sendData.Length.ToString());
                req.Content.Headers.TryAddWithoutValidation("x-pipeline-sequence-id", sequenceId.ToString());
                if (!string.IsNullOrEmpty(server_key))
                    req.Content.Headers.TryAddWithoutValidation("x-pipeline-server-key", server_key);
                if (!string.IsNullOrEmpty(affinity_key))
                    req.Content.Headers.TryAddWithoutValidation("x-pipeline-affinity-key", affinity_key);
                CancellationTokenSource cancellationTokenSource = new CancellationTokenSource(20000);
                HttpResponseMessage rsp = await SimpleHttpClient.SendAsync(req, HttpCompletionOption.ResponseHeadersRead, cancellationTokenSource.Token);
                if (rsp.IsSuccessStatusCode)
                {
                    byte[] retData = await rsp.Content.ReadAsByteArrayAsync();
                    ParseResponse(retData);
                    if (string.IsNullOrEmpty(server_key))
                    {
                        rsp.Headers.TryGetValues("x-pipeline-server-key", out IEnumerable<string> list);
                        var e = list.GetEnumerator();
                        e.MoveNext();
                        server_key = e.Current;
                    }
                    if (string.IsNullOrEmpty(affinity_key))
                    {
                        rsp.Headers.TryGetValues("x-pipeline-affinity-key", out IEnumerable<string> list);
                        var e = list.GetEnumerator();
                        e.MoveNext();
                        affinity_key = e.Current;
                    }
                    action?.Invoke();
                }
                else
                {
                    Debug.LogWarning($"SimplePostAsync Response Failed, StatusCode={rsp.StatusCode.ToString()}");
                }
            }
            catch (Exception e)
            {
                string exceptionType = "UnknownError";
                int statusCode = 0;
                if (e is ArgumentNullException)
                {
                    exceptionType = "ArgumentNull";
                }
                else if (e is InvalidOperationException)
                {
                    exceptionType = "InvalidOperation";
                }
                else if (e is HttpRequestException)
                {
                    if (e.InnerException is WebException)
                    {
                        WebException webException = e.InnerException as WebException;
                        exceptionType = webException.Status.ToString();
                        statusCode = (int)webException.Status;
                    }
                    else
                    {
                        HttpRequestException httpRequestException = e as HttpRequestException;
                        exceptionType = "HttpRequestException";
                        statusCode = httpRequestException.HResult;
                    }
                }
                else if (e is TaskCanceledException)
                {
                    exceptionType = "RequestTimeout";
                }
                Debug.LogWarning($"SimplePostAsync throw exception:{exceptionType}, code={statusCode.ToString()}");
            }
        }

        public void OnClickEffciency()
        {
            startTime = Time.realtimeSinceStartup;
            ResponseCount = 0;
            txt_cost_time.text = string.Empty;
            txt_rsp_count.text = string.Empty;
            RequestCount = int.Parse(Input_send_count.text);
            MockMessageReq req = new MockMessageReq
            {
                Type = MockType.MockNormal,
                NetScheme = HttpScheme
            };
            if (serverKeepOrder)
            {
                if (RequestCount > 0)
                {
                    if (HttpScheme.Equals("UnityWebRequest"))
                        UnityPostServerPipeline(req, PostRemain);
                    else if (HttpScheme.Equals("HttpClient"))
                        SimplePostServerPipelineAsync(req, PostRemain);
                }
            }
            else
            {
                for (int i = 0; i < RequestCount; i++)
                {
                    MockTestEntry.HttpClient.PostRequestMsg(URL, req);
                }
            }
        }

        public void OnKeepOrderChange(int v)
        {
            if (Dropdown_KeepOrder.value == 0)
            {
                serverKeepOrder = false;
            }
            else if (Dropdown_KeepOrder.value == 1)
            {
                serverKeepOrder = true;
            }
        }

        private void PostRemain()
        {
            MockMessageReq req = new MockMessageReq
            {
                Type = MockType.MockNormal,
                NetScheme = HttpScheme
            };
            for (int i = 0; i < RequestCount - 1; i++)
            {
                if (HttpScheme.Equals("UnityWebRequest"))
                    UnityPostServerPipeline(req);
                else if (HttpScheme.Equals("HttpClient"))
                    SimplePostServerPipelineAsync(req);
            }
        }

        private bool hasSendTask = false;
        public void OnClickSimulate()
        {
            RequestCount = int.Parse(Input_simulate_num.text);
            sendTaskTime = float.Parse(Input_simulate_time.text);
            if (RequestCount > 0 && sendTaskTime > 0)
            {
                if (serverKeepOrder)
                {
                    startTime = Time.realtimeSinceStartup;
                    ResponseCount = 0;
                    txt_cost_time.text = string.Empty;
                    txt_rsp_count.text = string.Empty;
                    MockMessageReq req = new MockMessageReq
                    {
                        Type = MockType.MockNormal,
                        NetScheme = HttpScheme
                    };
                    sendCount = 1;
                    if (HttpScheme.Equals("UnityWebRequest"))
                        UnityPostServerPipeline(req, PostSimulateRemain);
                    else if (HttpScheme.Equals("HttpClient"))
                        SimplePostServerPipelineAsync(req, PostSimulateRemain);
                }
                else
                {
                    hasSendTask = true;
                    sendCount = 0;
                    startTime = Time.realtimeSinceStartup;
                    ResponseCount = 0;
                    txt_cost_time.text = string.Empty;
                    txt_rsp_count.text = string.Empty;
                }
            }
        }

        private void PostSimulateRemain()
        {
            hasSendTask = true;
        }

        private void Update()
        {
            if (hasSendTask && RequestCount > sendCount)
            {
                UpdateSendTask();
            }
        }

        private float sendTaskTime;
        private int sendCount;
        private void UpdateSendTask()
        {
            if (Time.realtimeSinceStartup - startTime < sendTaskTime)
            {
                var t = Time.realtimeSinceStartup - startTime;
                var p = t / sendTaskTime * RequestCount;
                int targetCount = Mathf.RoundToInt(p);
                int count = targetCount - sendCount;
                //Debug.Log($"send count={count}, time={t}");
                MockMessageReq req = new MockMessageReq
                {
                    Type = MockType.MockNormal,
                    NetScheme = HttpScheme
                };
                if (serverKeepOrder)
                {
                    for (int i = 0; i < count; i++)
                    {
                        sendCount++;
                        if (HttpScheme.Equals("UnityWebRequest"))
                            UnityPostServerPipeline(req);
                        else if (HttpScheme.Equals("HttpClient"))
                            SimplePostServerPipelineAsync(req);
                    }
                }
                else
                {
                    for (int i = 0; i < count; i++)
                    {
                        sendCount++;
                        MockTestEntry.HttpClient.PostRequestMsg(URL, req);
                    }
                }
            }
            else
            {
                var t = Time.realtimeSinceStartup - startTime;
                MockMessageReq req = new MockMessageReq
                {
                    Type = MockType.MockNormal,
                    NetScheme = HttpScheme
                };
                int count = RequestCount - sendCount;
                //Debug.Log($"send count={count}, time={t}");
                if (serverKeepOrder)
                {
                    for (int i = 0; i < count; i++)
                    {
                        sendCount++;
                        if (HttpScheme.Equals("UnityWebRequest"))
                            UnityPostServerPipeline(req);
                        else if (HttpScheme.Equals("HttpClient"))
                            SimplePostServerPipelineAsync(req);
                    }
                }
                else
                {
                    for (int i = 0; i < count; i++)
                    {
                        sendCount++;
                        MockTestEntry.HttpClient.PostRequestMsg(URL, req);
                    }
                }
                hasSendTask = false;
            }
        }

        public void OnClickCookiesTest()
        {
            startTime = Time.realtimeSinceStartup;
            ResponseCount = 0;
            txt_cost_time.text = string.Empty;
            txt_rsp_count.text = string.Empty;
            RequestCount = int.Parse(Input_send_count.text);
            if (string.IsNullOrEmpty(session_key))
                session_key = Guid.NewGuid().ToString();
            cookies = string.Empty;
            MockMessageReq req = new MockMessageReq
            {
                Type = MockType.MockNormal,
                NetScheme = HttpScheme
            };
            for (int i = 0; i < RequestCount; i++)
            {
                MockTestEntry.HttpClientDisorder.PostRequestMsg(URL_Cookies, req, delegate(IMessage rsp)
                {
                    sb.Length = 0;
                    sb.Append(txt_response.text);
                    sb.AppendLine($"Response MockMessageRsp, {rsp}");
                    txt_response.text = sb.ToString();
                });
            }
            //MockTestEntry.HttpClientDisorder.PostRequestMsg(URL_Cookies, req, OnPostCookies);
            //if (HttpScheme.Equals("UnityWebRequest"))
            //    UnityPostServerPipelineCookies(req, PostCookiesRemain);
            //else if (HttpScheme.Equals("HttpClient"))
            //    SimplePostServerPipelineCookiesAsync(req, PostCookiesRemain);
        }

        private void UnityPostServerPipelineCookies(IMessage msg, Action action = null)
        {
            StartCoroutine(UnityPostServerPipelineCookiesCoroutine(msg, action));
        }

        private IEnumerator UnityPostServerPipelineCookiesCoroutine(IMessage msg, Action action = null)
        {
            int sequenceId = ProtokitUtil.Instance.GetSequenceId();
            byte[] sendData = ConverToBytes(msg, sequenceId);
            UnityWebRequest webReq = new UnityWebRequest(URL_Cookies, "POST")
            {
                uploadHandler = new UploadHandlerRaw(sendData)
            };
            webReq.timeout = 20;
            webReq.SetRequestHeader("Content-Type", "application/x-protobuf");
            webReq.SetRequestHeader("x-fun-request-id", sequenceId.ToString());
            webReq.SetRequestHeader("x-pipeline-sequence-id", sequenceId.ToString());
            webReq.SetRequestHeader("x-pipeline-session-key", session_key);
            if (!string.IsNullOrEmpty(server_key))
                webReq.SetRequestHeader("x-pipeline-server-key", server_key);
            if (!string.IsNullOrEmpty(cookies))
                webReq.SetRequestHeader("Cookie", cookies);
            webReq.downloadHandler = new DownloadHandlerBuffer();
            yield return webReq.SendWebRequest();
            if (webReq.isNetworkError || webReq.isHttpError)
            {
                Debug.LogWarning($"PostAtOnce Request failed. statusCode={webReq.responseCode}. error={webReq.error}");
            }
            else
            {
                byte[] retData = webReq.downloadHandler.data;
                ParseResponse(retData);
                var serverKey = webReq.GetResponseHeader("x-pipeline-server-key");
                Debug.Log("get server key:" + serverKey);
                if (string.IsNullOrEmpty(server_key))
                    server_key = webReq.GetResponseHeader("x-pipeline-server-key");
                if (string.IsNullOrEmpty(cookies))
                    cookies = webReq.GetResponseHeader("set-cookie");
                action?.Invoke();
            }
        }

        private async void SimplePostServerPipelineCookiesAsync(IMessage msg, Action action = null)
        {
            try
            {
                int sequenceId = ProtokitUtil.Instance.GetSequenceId();
                byte[] sendData = ConverToBytes(msg, sequenceId);
                HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, URL_Cookies)
                {
                    Content = new ByteArrayContent(sendData)
                };
                req.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-protobuf");
                req.Content.Headers.TryAddWithoutValidation("Connection", "keep-alive");
                req.Content.Headers.TryAddWithoutValidation("x-fun-request-id", sequenceId.ToString());
                req.Content.Headers.TryAddWithoutValidation("Content-Length", sendData.Length.ToString());
                req.Content.Headers.TryAddWithoutValidation("x-pipeline-sequence-id", sequenceId.ToString());
                req.Content.Headers.TryAddWithoutValidation("x-pipeline-session-key", session_key);
                if (!string.IsNullOrEmpty(server_key))
                    req.Content.Headers.TryAddWithoutValidation("x-pipeline-server-key", server_key);
                if (!string.IsNullOrEmpty(cookies))
                    req.Content.Headers.TryAddWithoutValidation("Cookie", cookies);
                CancellationTokenSource cancellationTokenSource = new CancellationTokenSource(20000);
                HttpResponseMessage rsp = await SimpleHttpClient.SendAsync(req, HttpCompletionOption.ResponseHeadersRead, cancellationTokenSource.Token);
                if (rsp.IsSuccessStatusCode)
                {
                    byte[] retData = await rsp.Content.ReadAsByteArrayAsync();
                    ParseResponse(retData);
                    if (string.IsNullOrEmpty(server_key))
                    {
                        rsp.Headers.TryGetValues("x-pipeline-server-key", out IEnumerable<string> list);
                        var e = list.GetEnumerator();
                        e.MoveNext();
                        server_key = e.Current;
                    }
                    if (string.IsNullOrEmpty(cookies))
                    {
                        rsp.Headers.TryGetValues("set-cookie", out IEnumerable<string> list);
                        var e = list.GetEnumerator();
                        e.MoveNext();
                        cookies = e.Current;
                        Debug.Log("Get Server Cookies:" + cookies);
                    }
                    action?.Invoke();
                }
                else
                {
                    Debug.LogWarning($"SimplePostAsync Response Failed, StatusCode={rsp.StatusCode.ToString()}");
                }
            }
            catch (Exception e)
            {
                string exceptionType = "UnknownError";
                int statusCode = 0;
                if (e is ArgumentNullException)
                {
                    exceptionType = "ArgumentNull";
                }
                else if (e is InvalidOperationException)
                {
                    exceptionType = "InvalidOperation";
                }
                else if (e is HttpRequestException)
                {
                    if (e.InnerException is WebException)
                    {
                        WebException webException = e.InnerException as WebException;
                        exceptionType = webException.Status.ToString();
                        statusCode = (int)webException.Status;
                    }
                    else
                    {
                        HttpRequestException httpRequestException = e as HttpRequestException;
                        exceptionType = "HttpRequestException";
                        statusCode = httpRequestException.HResult;
                    }
                }
                else if (e is TaskCanceledException)
                {
                    exceptionType = "RequestTimeout";
                }
                Debug.LogWarning($"SimplePostAsync throw exception:{exceptionType}, code={statusCode.ToString()}");
            }
        }

        private void PostCookiesRemain()
        {
            MockMessageReq req = new MockMessageReq
            {
                Type = MockType.MockNormal,
                NetScheme = HttpScheme
            };
            for (int i = 0; i < RequestCount - 1; i++)
            {
                if (HttpScheme.Equals("UnityWebRequest"))
                    UnityPostServerPipelineCookies(req);
                else if (HttpScheme.Equals("HttpClient"))
                    SimplePostServerPipelineCookiesAsync(req);
            }
        }
    }
}