﻿using UnityEngine;
using System;
using System.Collections.Generic;
using CenturyGame.AssetBundleManager.Runtime;

namespace MockTest
{
    public static class WaitDialogMgr
    {
        private static UnityEngine.Object prefab;
        private static Transform waitRoot;
        private static GameObject gameObject;

        public static void ShowWaitDialog()
        {
            if (prefab == null)
            {
                string prefabPath = "ui/waitdialog";
                prefab = AssetBundleManager.Load(prefabPath, typeof(UnityEngine.Object));
            }
            if (waitRoot == null)
            {
                waitRoot = GameObject.Find("UIRoot/Top").transform;
            }
            if (gameObject == null)
            {
                gameObject = UnityEngine.Object.Instantiate(prefab) as GameObject;
                gameObject.transform.SetParent(waitRoot);
                gameObject.name = "waitdialog";
            }
            else
            {
                if (!gameObject.activeSelf)
                    gameObject.SetActive(true);
            }
        }

        public static void CloseWaitDialog()
        {
            if (gameObject != null && gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
        }
    }
}