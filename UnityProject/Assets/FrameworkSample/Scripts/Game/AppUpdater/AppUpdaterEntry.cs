﻿
using CenturyGame.AppUpdaterLib.Runtime.Managers;
using CenturyGame.Framework;
using CenturyGame.Framework.Http;
using CenturyGame.Game;
using CenturyGame.Log4NetForUnity.Runtime;
using CenturyGame.LoggerModule.Runtime;
using UnityEngine;

public class AppUpdaterEntry : MonoBehaviour
{
    public static HttpManager Http
    {
        get;
        private set;
    }

    private AppUpdaterDemo mDemo;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(Application.persistentDataPath);
        LoggerManager.SetCurrentLoggerProvider(new Log4NetLoggerProvider());
        this.InitFrameworkModules();
        this.InitOthers();
    }

    // Update is called once per frame
    void Update()
    {
        FrameworkEntry.Update(Time.deltaTime, Time.unscaledDeltaTime);

        mDemo.Update();
    }

    private void LateUpdate()
    {
        FrameworkEntry.LateUpdate();
    }

    private void InitFrameworkModules()
    {
        Http = FrameworkEntry.GetModule<HttpManager>();
    }

    private void InitOthers()
    {
        var appUpdaterPanel = GameObject.Find("AppUpdaterPanel");
        mDemo = new AppUpdaterDemo(this, Http,appUpdaterPanel);
        mDemo.Init();
    }
    private void OnApplicationQuit()
    {
        FrameworkEntry.ShutDown();
        LoggerManager.Shutdown();
    }

    private void OnGUI()
    {
        if (GUILayout.Button("ManualStartAppUpdate"))
        {
            AppUpdaterManager.ManualStartAppUpdate();
        }

        if (GUILayout.Button("StartUpdateAgain"))
        {
            AppUpdaterManager.AppUpdaterStartUpdateAgain();
        }


        if (GUILayout.Button("Start retained data folder"))
        {
            AppUpdaterManager.AppUpdaterSetRetainedDataFolderName("textures");
            AppUpdaterManager.AppUpdaterSetRetainedDataFolderName("movies");
        }

        if (GUILayout.Button("Start Update movies"))
        {
            AppUpdaterManager.AppUpdaterBindFileUpdateRuleFilter((ref string n) =>
            {
                if (n.StartsWith("resource/movies/"))
                {
                    return false;
                }
                return true;
            });

            AppUpdaterManager.AppUpdaterStartDownloadPartialDataRes();
        }

        if (GUILayout.Button("Start Update textures"))
        {
            AppUpdaterManager.AppUpdaterBindFileUpdateRuleFilter((ref string n) =>
            {
                if (n.StartsWith("resource/textures/"))
                {
                    return false;
                }
                return true;
            });

            AppUpdaterManager.AppUpdaterStartDownloadPartialDataRes();
        }

        if (GUILayout.Button("Start Update ui"))
        {
            AppUpdaterManager.AppUpdaterBindFileUpdateRuleFilter((ref string n) =>
            {
                if (n.StartsWith("resource/ui/"))
                {
                    return false;
                }
                return true;
            });

            AppUpdaterManager.AppUpdaterStartDownloadPartialDataRes();
        }
    }

}
