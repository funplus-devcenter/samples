/***************************************************************

 *  类名称：        AppUpdaterDemo

 *  描述：				

 *  作者：          Chico(wuyuanbing)

 *  创建时间：      2020/5/26 17:30:13

 *  最后修改人：

 *  版权所有 （C）:   diandiangames

***************************************************************/

using System;
using System.Collections;
using CenturyGame.AppUpdaterLib.Runtime;
using CenturyGame.AppUpdaterLib.Runtime.Managers;
using CenturyGame.Framework.Http;
using CenturyGame.LoggerModule.Runtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace CenturyGame.Game
{
    public sealed class AppUpdaterDemo
    {
        //--------------------------------------------------------------
#region Fields
        //--------------------------------------------------------------

        private static readonly LoggerModule.Runtime.ILogger s_mLogger = LoggerManager.GetLogger("AppUpdaterDemo");

#endregion

        //--------------------------------------------------------------
#region Properties & Events
        //--------------------------------------------------------------

#endregion

        //--------------------------------------------------------------
#region Creation & Cleanup
        //--------------------------------------------------------------

        private GameObject mContent;
        private Slider mProgressBar;
        private AppUpdaterProgressData mProgressData;
        private MonoBehaviour mDriver;
        private HttpManager mHttpManager;
        public AppUpdaterDemo(MonoBehaviour driver, HttpManager httpManager, GameObject go)
        {
            this.mDriver = driver;
            this.mHttpManager = httpManager;
            this.mContent = go;
            this.mProgressBar = go.transform.Find("Progress").GetComponent<Slider>();
        }

#endregion

        //--------------------------------------------------------------
#region Methods
        //--------------------------------------------------------------

        public void Init()
        {
            AppUpdaterManager.Init();
            AppUpdaterManager.AppUpdaterSetAppUpdaterRequester(new AppUpdaterHttpRequester(this.mHttpManager));
            AppUpdaterManager.AppUpdaterSetStorageInfoProvider(new StorageInfoProvider());
            AppUpdaterManager.AppUpdaterHint(AppUpdaterHintName.MANUAL_PERFORM_APP_UPDATE,(int)AppUpdaterBool.TRUE);
            //AppUpdaterManager.AppUpdaterHint(AppUpdaterHintName.ENABLE_TABLE_DATA_UPDATE, (int)AppUpdaterBool.FALSE);
            AppUpdaterManager.AppUpdaterHint(AppUpdaterHintName.ENABLE_RES_INCREMENTAL_UPDATE, (int)AppUpdaterBool.TRUE);
            this.AddListeners();
            this.mProgressData = AppUpdaterManager.AppUpdaterGetAppUpdaterProgressData();
        }

        private void AddListeners()
        {
            AppUpdaterManager.AppUpdaterSetErrorCallback((errorType, desc) => { s_mLogger.Error(
                $"Error type {errorType} error message : {desc}" ); });

            AppUpdaterManager.AppUpdaterSetForceUpdateCallback(info =>
            {
                s_mLogger.Info("Client need to force update app!");
                s_mLogger.Info($"Versoin : {info.Versoin} PackageUrl : {info.PackageUrl} AppStoreUrl : {info.AppStoreUrl} DescUrl : {info.DescUrl}");
            });

            AppUpdaterManager.AppUpdaterSetOnTargetVersionObtainCallback(version =>
            {
                s_mLogger.Info($"Target version : {version} .");
            });

            AppUpdaterManager.AppUpdaterSetPerformCompletedCallback(() =>
            {
                s_mLogger.Info("AppUpdater perform completed");
                s_mLogger.Info($"server url : \"{AppUpdaterManager.AppUpdaterGetServerUrl()}\" .");
                s_mLogger.Info($"Channel : {AppUpdaterManager.AppUpdaterGetChannel()} .");
#if UNITY_EDITOR
                this.mProgressBar.gameObject.SetActive(false);

                //this.mDriver.StartCoroutine(this.LoadYourAsyncScene());
#endif

            });

            AppUpdaterManager.AppUpdaterSetServerMaintenanceCallback(maintenceInfo =>
            {
                s_mLogger.Info($"Maintence IsOpen : {maintenceInfo.IsOpen} UrlPattern : {maintenceInfo.UrlPattern}");
            });
        }

#if UNITY_EDITOR

        IEnumerator LoadYourAsyncScene()
        {
            LoadSceneParameters _params = new LoadSceneParameters(LoadSceneMode.Single);
            AsyncOperation asyncLoad = EditorSceneManager.LoadSceneAsyncInPlayMode("Assets/Scenes/UIDemo", _params);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }

        }
#endif

        public void Update()
        {
            if (this.mProgressBar)
            {
                this.mProgressBar.value = this.mProgressData.Progress;
            }
        }

#endregion

    }
}
