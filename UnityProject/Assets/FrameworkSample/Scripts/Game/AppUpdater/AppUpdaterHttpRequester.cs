/***************************************************************

 *  类名称：        AppUpdaterHttpRequester

 *  描述：				

 *  作者：          Chico(wuyuanbing)

 *  创建时间：      2020/5/19 16:21:59

 *  最后修改人：

 *  版权所有 （C）:   diandiangames

***************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using CenturyGame.AppUpdaterLib.Runtime;
using CenturyGame.AppUpdaterLib.Runtime.Configs;
using CenturyGame.AppUpdaterLib.Runtime.Interfaces;
using CenturyGame.AppUpdaterLib.Runtime.Managers;
using CenturyGame.AppUpdaterLib.Runtime.Protocols;
using CenturyGame.Core.Functional;
using CenturyGame.Framework.Http;
using CenturyGame.LoggerModule.Runtime;
using gen.msg;
using Google.Protobuf;
//using Msg;
using gen.netutils;
using UnityEngine;
using ILogger = CenturyGame.LoggerModule.Runtime.ILogger;

namespace CenturyGame.Game
{
    using IMessage = Google.Protobuf.IMessage;
    public class AppUpdaterHttpRequester : IAppUpdaterRequester
    {

        #region Enums & Inner Class

        public enum InnerState
        {
            Idle,

            StartRequest,

            Requesting,

            WaitForReq,

            RequestAgain,

            RequestingAgain,

            RequestCompleted,

            RequestFailure
        }

        #endregion

        //--------------------------------------------------------------
        #region Fields
        //--------------------------------------------------------------

        private static ILogger s_mLogger = LoggerManager.GetLogger("AppUpdaterHttpRequester");

        public HttpManager mHttpManager;

        private static int DefaultUrlsReqIndex = -1;

        private InnerState mInnerState = InnerState.Idle;

        /// <summary>
        /// 对应于EntryPointResponse返回
        /// </summary>
        private Action<GetVersionResponseInfo> mGetVersionInfoEvent = null;

        private GetVersionResponseInfo mReqResult = null;

        /// <summary>
        /// 请求的服务器的url列表，包括主服务器和fallback列表
        /// </summary>
        private string[] mUrlList = new string[0];

        /// <summary>
        /// 当前请求的
        /// </summary>
        private int mCurReqUrlIndex = DefaultUrlsReqIndex;

        private EntryPointRequest mCurRequest;
        private string mAppVersion;
        private string mChannel;

        private float mStartTime = 0;
        private float mReqInternal = 3f;

        #endregion

        //--------------------------------------------------------------
        #region Properties & Events
        //--------------------------------------------------------------

        public InnerState State => this.mInnerState;
        

        #endregion

        //--------------------------------------------------------------
        #region Creation & Cleanup
        //--------------------------------------------------------------

        public AppUpdaterHttpRequester(HttpManager httpManager)
        {
            this.mHttpManager = httpManager;
        }
       

        #endregion

        //--------------------------------------------------------------
        #region Methods
        //--------------------------------------------------------------

        public void Update()
        {
            switch (mInnerState)
            {
                case InnerState.StartRequest:
                    this.StartRequestToHttpServer();
                    break;
                case InnerState.WaitForReq:
                    this.WaitingForRequestToHttpServer();
                    break;
                case InnerState.RequestAgain:
                    this.StartRequestToHttpServerAgain();
                    break;
                case InnerState.RequestCompleted:
                    this.OnReqCompleted();
                    break;
                case InnerState.RequestFailure:
                    this.OnReqFailure();
                    break;
            }
        }

        public void ReqGetVersion(LighthouseConfig.Server serverData, string appVersion, string lighthouseId,string channel , FileServerType fromTo, Action<GetVersionResponseInfo> getVersionResponseInfoAction)
        {
            this.Clear();
            mUrlList = new string[serverData.FallbackUrlList.Count + 1];
            mUrlList[0] = serverData.Url;
            serverData.FallbackUrlList.ForCall((x, index) => { mUrlList[index+1] = x; });

            s_mLogger.Debug($"[appVersion : {appVersion}] [lighthouseId : {lighthouseId}] [channel : {channel}] [fromTo : {fromTo}] .");

            this.mAppVersion = appVersion;
            this.mChannel = channel;
            this.mGetVersionInfoEvent = getVersionResponseInfoAction;
            
            this.mCurRequest = new EntryPointRequest();
            this.mCurRequest.AppVersion = appVersion;
            this.mCurRequest.LighthouseId = lighthouseId;
            this.mCurRequest.From = (fromTo == FileServerType.CDN) ? EntryPointFromType.Cdn : EntryPointFromType.Oss;

            this.mInnerState = InnerState.StartRequest;
        }

        private void Clear()
        {
            this.mInnerState = InnerState.Idle;

            this.mGetVersionInfoEvent = null;

            this.mReqResult = null;

            this.mUrlList = new string[0];

            this.mCurReqUrlIndex= DefaultUrlsReqIndex;

            this.mCurRequest = null;
        }

        private void OnReqCompleted()
        {
            s_mLogger.Info("Request to http server complted!");
            this.mGetVersionInfoEvent?.Invoke(this.mReqResult);
            this.Clear();
        }

        private void OnReqFailure()
        {
            s_mLogger.Error("Request getVersiion failure!");

            this.mGetVersionInfoEvent?.Invoke(null);
            this.Clear();
        }

        private RawPacket GenRawPacket(IMessage msg, string passthrough)
        {
            var rp = new RawPacket
            {
            };
            using (var stream = new MemoryStream())
            {
                msg.WriteTo(stream);
                var rawAny = new RawAny
                {
                    Uri = msg.Descriptor.FullName,
                    Raw = ByteString.CopyFrom(stream.ToArray()),
                    PassThrough = passthrough,
                };
                rp.RawAny.Add(rawAny);
                rp.Version = 1;
            }
            return rp;

        }

        private byte[] GetRawPacketData(RawPacket rp)
        {
            using (var stream = new MemoryStream())
            {
                rp.WriteTo(stream);
                return stream.ToArray();
            }
        }


        private void StartRequestToHttpServer()
        {
            this.mInnerState = InnerState.Requesting;

            this.StartRequestToHttpServerInnernal();
        }

        private void StartRequestToHttpServerAgain()
        {
            this.mInnerState = InnerState.RequestingAgain;

            this.StartRequestToHttpServerInnernal();
        }


        private bool CheckEntryPointResponseValid(EntryPointResponse response)
        {
            s_mLogger.Debug($"forceUpdate : {response.ForceUpdate}");
            s_mLogger.Debug($"lighthouseId : {response.LighthouseId}");
            s_mLogger.Debug($"maintenance : {response.Maintenance}");

            if (string.IsNullOrEmpty(response.LighthouseId))
            {
                s_mLogger.Error("The LighthouseId that from entryPointResponse is null or empty!");
                return false;
            }

            if (response.ResourceDetail == null)
            {
                s_mLogger.Error("The resourceDeetail that from entryPointResponse is null!");
                return false;
            }

#if UNITY_ANDROID
            if (response.ResourceDetail.AndroidVersion == null)
            {
                s_mLogger.Error("The AndroidVersion that from entryPointResponse.ResourceDetail is null!");
                return false;
            }
#elif UNITY_IPHONE
            if (response.ResourceDetail.IOSVersion == null)
            {
                s_mLogger.Error("The IOSVersion that from entryPointResponse.ResourceDetail is null!");
                return false;
            }
#else
                        throw new InvalidOperationException($"Invalid platform : {Application.platform} .");
#endif
            return true;

        }

        private void SetMetadata(RawPacket rp)
        {
            rp.Metadata.Add(MetadataKey.AppVersion.ToString());
            rp.Metadata.Add(this.mAppVersion);

            rp.Metadata.Add(MetadataKey.Channel.ToString());
            rp.Metadata.Add(this.mChannel);

            rp.Metadata.Add(MetadataKey.Language.ToString());
            rp.Metadata.Add("en");

            var clientUniqueId = AppUpdaterManager.ClientUniqueId;
            if (!string.IsNullOrEmpty(clientUniqueId))
            {
                s_mLogger.Debug($"clientUniqueId : {clientUniqueId}");
                rp.Metadata.Add(MetadataKey.ClientId.ToString());
                rp.Metadata.Add(clientUniqueId);
            }
        }

        private void StartRequestToHttpServerInnernal()
        {
            mCurReqUrlIndex++;
            
            var rp = GenRawPacket(this.mCurRequest, (DateTime.Now).ToString("yyyyMMddHHmmssffff"));
            SetMetadata(rp);

            var header = new Dictionary<string, string>
            {
                {"Content-Type", "application/x-protobuf"}, {"charset", "utf-8"}
            };

            var curUrl = this.mUrlList[this.mCurReqUrlIndex] + "lighthouse";
            curUrl = OptimizeUrl(curUrl);
            s_mLogger.Info($"Current req url : {curUrl} !");
            this.mHttpManager.PostHttpRequest(curUrl, header, GetRawPacketData(rp), delegate (byte[] recvData)
            {
                if (recvData == null || recvData.Length == 0)
                {
                    this.mInnerState = InnerState.WaitForReq;
                    this.mStartTime = Time.realtimeSinceStartup;
                    return;
                }

                var receiveData = RawPacket.Parser.ParseFrom(recvData);
                if (ErrorResponse.Descriptor.FullName.Equals(receiveData.RawAny[0].Uri))
                {
                    var errorMsg = ErrorResponse.Parser.ParseFrom(receiveData.RawAny[0].Raw);
                    s_mLogger.Info($"ErrorResponse code : {errorMsg.Code}");
                    var lighthouse_id = new System.Text.UTF8Encoding(false, true).GetString(errorMsg.Data.ToByteArray());
                    GetVersionResponseInfo result = new GetVersionResponseInfo();
                    result.lighthouseId = lighthouse_id;                    
                    if (errorMsg.Code == APP_UPDATE_ERROR_CODE.MAINTENANCE)
                    {
                        result.maintenance = true;
                        this.mReqResult = result;
                        this.mInnerState = InnerState.RequestCompleted;
                    }
                    else if (errorMsg.Code == APP_UPDATE_ERROR_CODE.FORCE_UPDATE)
                    {
                        result.forceUpdate = true;
                        this.mReqResult = result;
                        this.mInnerState = InnerState.RequestCompleted;
                    }
                    else
                    {
                        this.mInnerState = InnerState.RequestFailure;
                    }
                }
                else
                {
                    var msg = EntryPointResponse.Parser.ParseFrom(receiveData.RawAny[0].Raw);
                    var redirectUrl = msg.RedirectURL;
                    if (!string.IsNullOrEmpty(msg.RedirectURL))
                    {
                        redirectUrl = OptimizeUrl(redirectUrl);
                        if (!string.Equals(redirectUrl, curUrl))
                        {
                            this.mUrlList = new string[] { redirectUrl };
                            this.mCurReqUrlIndex = DefaultUrlsReqIndex;
                            this.mStartTime = Time.realtimeSinceStartup;
                            this.mInnerState = InnerState.WaitForReq;
                            s_mLogger.Info($"{mReqInternal} seconds to redirect request , redirect url : {msg.RedirectURL} .");
                            return;
                        }
                    }

                    if (CheckEntryPointResponseValid(msg))
                    {
                        GetVersionResponseInfo result = new GetVersionResponseInfo();
                        result.url = curUrl;
                        result.forceUpdate = msg.ForceUpdate;
                        result.lighthouseId = msg.LighthouseId;
                        result.maintenance = msg.Maintenance;

                        result.update_detail = new ResUpdateDetail();
                        
                        s_mLogger.Debug($"DataVersion : {msg.ResourceDetail.DataVersion}");
                        result.update_detail.DataVersion = msg.ResourceDetail.DataVersion;
#if UNITY_ANDROID
                        s_mLogger.Debug($"AndroidVersion : {msg.ResourceDetail.AndroidVersion}");
                        result.update_detail.ResVersionNum = msg.ResourceDetail.AndroidVersion.Version;
                        result.update_detail.AndroidVersion = msg.ResourceDetail.AndroidVersion.Md5;
#elif UNITY_IPHONE
                        s_mLogger.Debug($"IOSVersion : {msg.ResourceDetail.IOSVersion}");
                        result.update_detail.ResVersionNum = msg.ResourceDetail.IOSVersion.Version;
                        result.update_detail.IOSVersion = msg.ResourceDetail.IOSVersion.Md5;
#else
                throw new InvalidOperationException($"Invalid platform : {Application.platform} .");
#endif
                        this.mReqResult = result;
                        this.mInnerState = InnerState.RequestCompleted;
                    }
                    else
                    {
                        this.mInnerState = InnerState.RequestFailure;
                    }
                }
            });
        }


        private void WaitingForRequestToHttpServer()
        {
            if (Time.realtimeSinceStartup - this.mStartTime > this.mReqInternal)
            {
                this.mInnerState = InnerState.RequestAgain;
                s_mLogger.Info("Start to request again!");
            }
            else
            {
                if (!CheckCurrentUrlValid())
                {
                    this.mInnerState = InnerState.RequestFailure;
                }
            }
        }

        private string OptimizeUrl(string url)
        {
            int length = url.Length;
            if (url[length - 1] == '/')
            {
                url = url.Substring(0,length -1);
            }

            return url;
        }

        private bool CheckCurrentUrlValid()
        {
            if (mCurReqUrlIndex >= this.mUrlList.Length - 1)
            {
                return false;
            }

            return true;
        }

#endregion
    }
}
